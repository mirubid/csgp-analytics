﻿using Autofac;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public class HangfireStartup : IStartup
    {
        public HangfireStartup(ILifetimeScope context)
        {
            GlobalConfiguration.Configuration.UseAutofacActivator(context);
        }
        public void Configure()
        {
            // noop (configured in constructor)
        }
    }
}
