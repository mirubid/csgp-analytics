﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public interface IMetricDataPointSaver
    {
        Task SaveAsync(Metric metric,MetricDataPoint datapoint, MetricData data);
    }
}
