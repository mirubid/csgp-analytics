﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using System.Collections.Generic;

namespace csgp.CentralizedAnalyticsProcessing
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureHangfire(IAppBuilder app)
        {
            
            app.UseHangfireDashboard("/jobs",new DashboardOptions()
            {
                AuthorizationFilters = new[] {new UnsafeAuthFilter()}
            });
           
        }

        class UnsafeAuthFilter : IAuthorizationFilter
        {
            bool IAuthorizationFilter.Authorize(IDictionary<string, object> owinEnvironment)
            {
                return true;
            }
        }
    }
}