﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    [DisplayColumn("Name")]
    public class Metric
    {
        public Metric()
        {
            CreateDateUTC = DateTime.UtcNow;
            UpdateDateUTC = CreateDateUTC;
        }
        [ReadOnly(true)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [ReadOnly(true)]
        [Required]
        public string TenantId { get; set; }
        /// <summary>
        /// where this data comes from (GA, SQL, etc)
        /// </summary>
        [ReadOnly(true)]
        [Required]
        public string DataSourceId { get; set; }

        public TenantDataSource DataSource { get; set; }

        [Required]
        public string DataDestinationId { get; set; }
        public TenantDataSource DataDestination { get; set; }
       
        /// <summary>
        /// Source specific configuration for this metric (JSON)
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Cron Expression
        /// see http://en.wikipedia.org/wiki/Cron#CRON_expression
        /// </summary>
        public string Schedule { get; set; }
        /// <summary>
        /// some statistics can't be calculated by aggragating more granular results
        /// e.g. monthly unique visitors can't be accurately calculated from daily unique visitors
        /// </summary>
        public bool EnableAggregation { get; set; }
        [ReadOnly(true)]
        public DateTime CreateDateUTC { get; set; }

        [ReadOnly(true)]
        public DateTime UpdateDateUTC { get; set; }
    }
}
