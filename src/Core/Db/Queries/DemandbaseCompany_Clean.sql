/****** Script for SelectTopNRows command from SSMS  ******/

  /*
   delete Demandbase info from MetricData_7_1 (use this after DemandbaseCompany_Merge)
  */
  CREATE PROCEDURE DemandbaseCompany_CleanData
  AS
	DELETE md
	FROM dbo.MetricDataPoint mdp
	JOIN dbo.MetricData_7_1 md
	  ON md.MetricDataPointId = mdp.Id
	Where mdp.MetricId IN (8,9,10,11,12)