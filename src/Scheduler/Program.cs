﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.SqlServer;

using Autofac;
using Autofac.Configuration;
using System.Configuration.Install;
using System.ServiceProcess;
using System.Reflection;

using NLog;

namespace csgp.CentralizedAnalytics
{
    class Program
    {
        internal static ILogger Logger = LogManager.GetCurrentClassLogger();

        private const string serviceName = "csgp.AnalyticsSchedulerService";
        static int Main(string[] args)
        {
            
            if (args.Length > 1 && args[0] == "service") {
                RunServiceControl(args);
                return 0;
            }

            if(args.Length > 0 && args[0] == "run-service")
            {
                RunAsService();
                return 0;
            }


            RunAsApp();
            

            return 0;
        }
        private static void RunServiceControl(string[] args)
        {
            Logger.Info("running service control {0} {1}", args);
            switch (args[1])
            {
                case "install":
                    ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                    break;
                case "uninstall":
                    ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                    break;
                case "status":
                    Console.WriteLine(new ServiceController() { ServiceName = serviceName }.Status);
                    break;
                case "start":
                    try
                    {
                        new ServiceController() { ServiceName = serviceName }.Start();
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex);
                    }
                    break;
                case "stop":
                    try
                    {
                        new ServiceController() { ServiceName = serviceName }.Stop();
                    }catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex);
                    }
                    break;

            }
        }
        private static void RunAsApp()
        {
            Logger.Info("running as app");
            using (new SchedulerRunner())
            {

                Console.WriteLine("press any key to quit");

                Console.ReadKey();
            }
        }
        private static  void RunAsService()
        {
            Logger.Info("running as service");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new AnalyticsSchedulerService()
            };
            ServiceBase.Run(ServicesToRun);
        }
        
    }
}
