﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace csgp.CentralizedAnalytics
{
    [DisplayColumn("Id")]
    public class Tenant
    {
        public Tenant()
        {
            CreateDateUTC = DateTime.UtcNow;// avoid datetime conversion error caused by DateTime.MinValue
            UpdateDateUTC = CreateDateUTC;
        }
        [StringLength(50)]
        [Required][ReadOnly(true)]
        public string Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Metric> Metrics { get; set; }

        [DisplayName("Create Date (UTC)")]
        [ReadOnly(true)]
        public DateTime CreateDateUTC { get; set; }

        [ReadOnly(true), DisplayName("Update Date (UTC)")]
        public DateTime UpdateDateUTC { get; set; }

        public virtual ICollection<TenantDataSource> DataSources { get; set; }

    }
}
