﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
    public class TenantController : ApiController
    {
        private csgp.CentralizedAnalytics.EF.AnalyticsDbContext db = new csgp.CentralizedAnalytics.EF.AnalyticsDbContext();

        public IQueryable<TenantDTO> GetTenants(int pageSize = 10
                )
        {
            var model = db.Tenants.AsQueryable();
                        
            return model.Select(TenantDTO.SELECT).Take(pageSize);
        }

        [HttpGet]
        [System.Web.Http.Route("api/Tenant_DataSources")]
        public IHttpActionResult Tenant_DataSources([FromUri]System.String id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            var parent = new Tenant()
            {
                Id = id,
            };
            var t = new Tenant() { Id = id };
            db.Tenants.Attach(t);

            //var tenant=db.Tenants.Where(e=>e.Id==id).FirstOrDefault();
            //if (tenant == null)
            //{
            //    return NotFound();
            //}
            
            return Ok(db.TenantDataSources.Where(e => e.Tenant.Id == t.Id)
                .Select(TenantDataSourceDTO.SELECT));
            
        }

        [ResponseType(typeof(TenantDTO))]
        public async Task<IHttpActionResult> GetTenant(string id)
        {
            var model = await db.Tenants.Select(TenantDTO.SELECT).FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        public async Task<IHttpActionResult> PutTenant(string id, Tenant model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TenantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(TenantDTO))]
        public async Task<IHttpActionResult> PostTenant(Tenant model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tenants.Add(model);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TenantExists(model.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            var ret = await db.Tenants.Select(TenantDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return CreatedAtRoute("DefaultApi", new { id = model.Id }, model);
        }

        [ResponseType(typeof(TenantDTO))]
        public async Task<IHttpActionResult> DeleteTenant(string id)
        {
            Tenant model = await db.Tenants.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.Tenants.Remove(model);
            await db.SaveChangesAsync();
            var ret = await db.Tenants.Select(TenantDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return Ok(ret);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TenantExists(string id)
        {
            return db.Tenants.Count(e => e.Id == id) > 0;
        }
    }
}