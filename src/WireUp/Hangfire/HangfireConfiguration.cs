﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Hangfire;
using Hangfire.SqlServer;

namespace csgp.CentralizedAnalytics
{
    public class HangfireConfiguration:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<HangfireJobEnquer>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.Register<BackgroundJobServer>(e =>
            {
                return ConfigureHangfire(e.Resolve<IComponentContext>());
            }).SingleInstance();

            builder.RegisterType<HangfireStartup>().As<IStartup>().SingleInstance();
            var json_settings = new Newtonsoft.Json.JsonSerializerSettings()
            {
                DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind
            };

            // set up hangfire
            var options = new SqlServerStorageOptions()
                        {
                            QueuePollInterval = TimeSpan.FromSeconds(5),
                            PrepareSchemaIfNecessary=false
                        };
            GlobalConfiguration.Configuration.UseSqlServerStorage("hangfire", options);
            Newtonsoft.Json.JsonConvert.DefaultSettings = () => json_settings;

            Hangfire.Common.JobHelper.SetSerializerSettings(json_settings);

            
        }
        public BackgroundJobServer ConfigureHangfire(IComponentContext context) {
            
            

            GlobalConfiguration.Configuration.UseAutofacActivator(context.Resolve<ILifetimeScope>());

            return new BackgroundJobServer();
        }
    }
}
