USE [MarketPlaceEcomSystemAnalytic]

EXEC dbo.DemandbaseCompany_Merge
GO

/*
merge data from demandbase metric data views into dbo.DemandbaseCompany

*/
CREATE PROCEDURE dbo.DemandbaseCompany_Merge
AS
BEGIN
	MERGE INTO dbo.DemandbaseCompany target
	USING (
		select 
			[ga:dimension6],
			Max([ga:dimension7]) as [ga:dimension7],
			Max([ga:dimension8]) as [ga:dimension8],
			Max([ga:dimension9]) as [ga:dimension9],
			Max([ga:dimension10]) as [ga:dimension10],
			Max([ga:dimension11]) as [ga:dimension11],
			Max([ga:dimension12]) as [ga:dimension12]

		from [dbo].[MetricData_8_View] where [ga:dimension6]<>'(Non-Company Visitor)'
		group by ([ga:dimension6])

	) as source
	ON source.[ga:dimension6]=target.DemandbaseSID
	WHEN MATCHED
		AND CHECKSUM(target.[ga:dimension7],target.[ga:dimension8],target.[ga:dimension9],target.[ga:dimension10],target.[ga:dimension11],target.[ga:dimension12]) <> CHECKSUM(source.[ga:dimension7], source.[ga:dimension8], source.[ga:dimension9], source.[ga:dimension10], source.[ga:dimension11], source.[ga:dimension12])
	 THEN
		UPDATE SET
			target.[ga:dimension7]=source.[ga:dimension7],
			target.[ga:dimension8]=source.[ga:dimension8],
			target.[ga:dimension9]=source.[ga:dimension9],
			target.[ga:dimension10]=source.[ga:dimension10],
			target.[ga:dimension11]=source.[ga:dimension11],
			target.[ga:dimension12]=source.[ga:dimension12],
			target.UtcUpdateDate=SysUtcDateTime()
	WHEN NOT MATCHED
	 THEN
		INSERT
		(DemandbaseSID,[ga:dimension7], [ga:dimension8], [ga:dimension9], [ga:dimension10], [ga:dimension11], [ga:dimension12],UtcCreateDate)
		VALUES
		([ga:dimension6], [ga:dimension7], [ga:dimension8], [ga:dimension9], [ga:dimension10], [ga:dimension11], [ga:dimension12],SysUtcDateTime())
	;


	MERGE INTO dbo.DemandbaseCompany target
	USING (
		select 
			[ga:dimension6],
			Max([ga:dimension13]) as [ga:dimension13],
			Max([ga:dimension14]) as [ga:dimension14],
			Max([ga:dimension15]) as [ga:dimension15],
			Max([ga:dimension16]) as [ga:dimension16],
			Max([ga:dimension17]) as [ga:dimension17],
			Max([ga:dimension18]) as [ga:dimension18]

		from [dbo].[MetricData_9_View] where [ga:dimension6]<>'(Non-Company Visitor)'
		group by ([ga:dimension6])
	)
	as source
	ON source.[ga:dimension6]=target.DemandbaseSID
	WHEN MATCHED
	AND Checksum(target.[ga:dimension13],target.[ga:dimension14],target.[ga:dimension15],target.[ga:dimension16],target.[ga:dimension17],target.[ga:dimension18])<>Checksum(source.[ga:dimension13], source.[ga:dimension14], source.[ga:dimension15], source.[ga:dimension16], source.[ga:dimension17], source.[ga:dimension18])
	THEN UPDATE SET
		target.[ga:dimension13]=source.[ga:dimension13],
		target.[ga:dimension14]=source.[ga:dimension14],
		target.[ga:dimension15]=source.[ga:dimension15],
		target.[ga:dimension16]=source.[ga:dimension16],
		target.[ga:dimension17]=source.[ga:dimension17],
		target.[ga:dimension18]=source.[ga:dimension18],
		target.UtcUpdateDate = SysUtcDateTime()
	WHEN NOT MATCHED
	THEN INSERT
	(DemandbaseSID,[ga:dimension13], [ga:dimension14], [ga:dimension15], [ga:dimension16], [ga:dimension17], [ga:dimension18],UtcCreateDate)
	VALUES
	([ga:dimension6],[ga:dimension13], [ga:dimension14], [ga:dimension15], [ga:dimension16], [ga:dimension17], [ga:dimension18],SysUtcDateTime())
	;

	MERGE INTO dbo.DemandbaseCompany target
	USING (
	select 
			[ga:dimension6],
			Max([ga:dimension19]) as [ga:dimension19],
			Max([ga:dimension20]) as [ga:dimension20],
			Max([ga:dimension21]) as [ga:dimension21],
			Max([ga:dimension22]) as [ga:dimension22],
			Max([ga:dimension23]) as [ga:dimension23],
			Max([ga:dimension24]) as [ga:dimension24]

		from [dbo].[MetricData_10_View] where [ga:dimension6]<>'(Non-Company Visitor)'
		group by ([ga:dimension6])
	) source
	ON source.[ga:dimension6]=target.DemandbaseSID
	WHEN MATCHED
		AND Checksum(target.[ga:dimension19],target.[ga:dimension20],target.[ga:dimension21],target.[ga:dimension22],target.[ga:dimension23],target.[ga:dimension24]) <> Checksum(source.[ga:dimension19], source.[ga:dimension20], source.[ga:dimension21], source.[ga:dimension22], source.[ga:dimension23], source.[ga:dimension24])
	THEN UPDATE SET
		target.[ga:dimension19]=source.[ga:dimension19],
		target.[ga:dimension20]=source.[ga:dimension20],
		target.[ga:dimension21]=source.[ga:dimension21],
		target.[ga:dimension22]=source.[ga:dimension22],
		target.[ga:dimension23]=source.[ga:dimension23],
		target.[ga:dimension24]=source.[ga:dimension24],
		target.UtcUpdateDate = SysUtcDateTime()
	WHEN NOT MATCHED THEN
	INSERT
	(DemandbaseSID,[ga:dimension19], [ga:dimension20], [ga:dimension21], [ga:dimension22], [ga:dimension23], [ga:dimension24],UtcCreateDate)
	VALUES
	([ga:dimension6],source.[ga:dimension19], source.[ga:dimension20], source.[ga:dimension21], source.[ga:dimension22], source.[ga:dimension23], source.[ga:dimension24],SysUtcDateTime())
	;

	MERGE INTO dbo.DemandbaseCompany target
	USING (
	select 
			[ga:dimension6],
			Max([ga:dimension25]) as [ga:dimension25],
			Max([ga:dimension26]) as [ga:dimension26],
			Max([ga:dimension27]) as [ga:dimension27],
			Max([ga:dimension28]) as [ga:dimension28],
			Max([ga:dimension29]) as [ga:dimension29],
			Max([ga:dimension30]) as [ga:dimension30]

		from [dbo].[MetricData_11_View] where [ga:dimension6]<>'(Non-Company Visitor)'
		group by ([ga:dimension6])
	) source
	ON source.[ga:dimension6]=target.DemandbaseSID
	WHEN MATCHED
		AND Checksum(target.[ga:dimension25],target.[ga:dimension26],target.[ga:dimension27],target.[ga:dimension28],target.[ga:dimension29],target.[ga:dimension30]) <> Checksum(source.[ga:dimension25], source.[ga:dimension26], source.[ga:dimension27], source.[ga:dimension28], source.[ga:dimension29], source.[ga:dimension30])
	THEN UPDATE SET
		target.[ga:dimension25]=source.[ga:dimension25],
		target.[ga:dimension26]=source.[ga:dimension26],
		target.[ga:dimension27]=source.[ga:dimension27],
		target.[ga:dimension28]=source.[ga:dimension28],
		target.[ga:dimension29]=source.[ga:dimension29],
		target.[ga:dimension30]=source.[ga:dimension30],
		target.UtcUpdateDate = SysUtcDateTime()
	WHEN NOT MATCHED THEN
	INSERT
	(DemandbaseSID,[ga:dimension25], [ga:dimension26], [ga:dimension27], [ga:dimension28], [ga:dimension29], [ga:dimension30],UtcCreateDate)
	VALUES
	([ga:dimension6],source.[ga:dimension25], source.[ga:dimension26], source.[ga:dimension27], source.[ga:dimension28], source.[ga:dimension29], source.[ga:dimension30],SysUtcDateTime())
	;

	MERGE INTO dbo.DemandbaseCompany target
	USING (
	select 
			[ga:dimension6],
			Max([ga:dimension31]) as [ga:dimension31],
			Max([ga:dimension32]) as [ga:dimension32],
			Max([ga:dimension33]) as [ga:dimension33]

		from [dbo].[MetricData_12_View] where [ga:dimension6]<>'(Non-Company Visitor)'
		group by ([ga:dimension6])
	) source
	ON source.[ga:dimension6]=target.DemandbaseSID
	WHEN MATCHED
		AND Checksum(target.[ga:dimension31],target.[ga:dimension32],target.[ga:dimension33]) <> Checksum(source.[ga:dimension31], source.[ga:dimension32], source.[ga:dimension33])
	THEN UPDATE SET
		target.[ga:dimension31]=source.[ga:dimension31],
		target.[ga:dimension32]=source.[ga:dimension32],
		target.[ga:dimension33]=source.[ga:dimension33],
		target.UtcUpdateDate = SysUtcDateTime()
	WHEN NOT MATCHED THEN
	INSERT
	(DemandbaseSID,[ga:dimension31], [ga:dimension32], [ga:dimension33],UtcCreateDate)
	VALUES
	([ga:dimension6],[ga:dimension31], [ga:dimension32], [ga:dimension33],SysUtcDateTime())
	;
END


