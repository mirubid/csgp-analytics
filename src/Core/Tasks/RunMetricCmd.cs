﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.Tasks
{
    public class QueueMetricCmd : ICmd
    {
        private string _days_ago;
        private string _metric_id;
        private string _date;
        private bool _force;

        private MetricTaskRunner _runner;
        ILogger _logger = LogManager.GetCurrentClassLogger();
        public QueueMetricCmd(MetricTaskRunner runner)
        {
            _runner = runner;

        }
        public string[] Names
        {
            get
            {
                return new[] { "queue-metric" };
            }
        }

        public void ConfigureOptions(ICmdConfigurator options)
        {
            options.AddOption("m|metric=", "metric id", (e) => _metric_id = e)
                .AddOption("da|days-ago=", "days ago", e => _days_ago = e)
                .AddOption("d|date=","specific date", e=> _date = e)
                .AddOption("f|force","force rerun metric even if it already ran on specified date", e=>_force=(e!=null))
                ;
        }

        public int Execute(List<string> extraArgs)
        {
            int daysAgo;
            int metric_id;
            DateTime date=DateTime.MinValue;

            if(!int.TryParse(_metric_id, out metric_id))
            {
                throw new ArgumentException("metric|m (int) must be specifice");
            }
            
            if (!string.IsNullOrEmpty(_date))
            {
                date = DateTime.Parse(_date);
            }else if (!string.IsNullOrEmpty(_days_ago) && int.TryParse(_days_ago, out daysAgo))
            {
                
                date = DateTime.UtcNow - TimeSpan.FromDays(daysAgo);
            }

            if (date == DateTime.MinValue)
            {
                throw new ArgumentException("date must be specifed.  Use either days-ago|da (int) or date|d (string).");
            }

            Console.WriteLine(string.Format("Queueing {0} for date {1}", metric_id, date));
            _logger.Info("Queueing {0} for date {1}", metric_id, date);

            


            _runner.EnqueueMetric(metric_id, date);


            return 0;
            
        }
    }
}
