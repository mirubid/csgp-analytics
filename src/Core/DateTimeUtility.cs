﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public static class DateTimeUtility
    {
        private static Regex _nDaysAgo = new Regex(@"^(\d+)daysago$", RegexOptions.Compiled);
        public static DateTime ParseAsDate(this string dateTimeString) {
            return ParseAsDate(dateTimeString, DateTime.UtcNow);
        }
        public static DateTime ParseAsDate(this string dateTimeString, DateTime now)
        {
            if (dateTimeString == null)
            {
                throw new ArgumentNullException("dateTimeString");
            }

            DateTime date;
            if(DateTime.TryParse(dateTimeString,out date))
            {
                return RemoveTime(date);
            }
            dateTimeString = dateTimeString.Trim().ToLower();

            if (dateTimeString == "yesterday")
            {

                return DaysAgo(now,1);
            }
            if (dateTimeString == "today")
            {
                return RemoveTime(now);
            }
            var m = _nDaysAgo.Match(dateTimeString);
            if (m.Success && m.Groups.Count==2)
            {
                int ago;
                if (int.TryParse(m.Groups[1].Value, out ago)){
                    return DaysAgo(now,ago);
                }
            }

            return RemoveTime(now);
        }
        public static DateTime RemoveTime(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, date.Kind);
        }
        public static DateTime FromDaysAgo(int daysAgo)
        {
            return DaysAgo(DateTime.UtcNow, daysAgo);
            
        }
        public static DateTime DaysAgo(this DateTime now,int daysAgo)
        {
            return RemoveTime(now - TimeSpan.FromDays(daysAgo));
        }
        /// <summary>
        /// convert to yyyy-MM-dd string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToYMD(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}
