﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace csgp.CentralizedAnalytics
{
    [DisplayColumn("Id")]
    public class TenantDataSource
    {
        public TenantDataSource()
        {
            CreateDateUTC = DateTime.UtcNow;// avoid datetime conversion error caused by DateTime.MinValue
            UpdateDateUTC = CreateDateUTC;
        }
        [ReadOnly(true)]
        public string Id { get; set; }

        [Required,ReadOnly(true)]
        public string TenantId { get; set; }  

        public Tenant Tenant { get; set; }

        [DisplayName("Data Source Configuration")]
        public string Configuration { get; set; }

        [Required]
        [ReadOnly(true)]
        public string DataSourceTypeId { get; set; }
        public DataSourceType DataSourceType { get; set; }

        public virtual ICollection<Metric> Metrics { get; set; }

        [DisplayName("Create Date (UTC)")]
        [Editable(false)]
        public DateTime CreateDateUTC { get; set; }

        [DisplayName("Update Date (UTC)")]
        [Editable(false)]
        public DateTime UpdateDateUTC { get; set; }
    }
}
