﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csgp.CentralizedAnalytics.EF;
using System.ComponentModel;
using NLog;

namespace csgp.CentralizedAnalytics.Tasks
{
    public class MetricTaskRunner
    {

        private Func<string, IMetricRetriever> _metricSource;
        private Func<string, IMetricDataPointSaver> _metricDestination;
        private IJobEnquer _queue;
        ILogger _logger = LogManager.GetCurrentClassLogger();

        public MetricTaskRunner(
            Func<string,IMetricRetriever> metricSource, 
            Func<string, IMetricDataPointSaver> metricDestination, 
            IJobEnquer queue)
        {
            _metricSource = metricSource;
            _metricDestination = metricDestination;
            _queue = queue;
        }

        [DisplayName("Enqueue Analytics Metric Job")]
        public void EnqueueMetric(int metricId)
        {
            /// enque the metric with a specific datetime so that any retries can generate the same query
            EnqueueMetric(metricId, DateTime.UtcNow.ToString("O"),0);
                

        }
        [DisplayName("Enqueue Analytics Metric Job")]
        public void EnqueueMetric(int metricId,DateTime utcNow,int pageIndex=0)
        {
            /// enque the metric with a specific datetime so that any retries can generate the same query
            EnqueueMetric(metricId, utcNow.ToString("O"),pageIndex);


        }
        private void EnqueueMetric(int metricId, string utcNow, int pageIndex = 0,int? metricDatapointId=null,bool force=false)
        {
            var request = new ExecuteMetricTaskRequest()
            {
                MetricId = metricId,
                UtcNow = utcNow,
                PageIndex = pageIndex,
                MetricDatapointId = metricDatapointId
            };
            using(var db=new EF.AnalyticsDbContext())
            {
                
                var metric= db.Metrics.Include("DataSource")
                    .Include("DataDestination")
                    .FirstOrDefault(e => e.Id == metricId);

                if (metric == null)
                {
                    throw new ArgumentException(String.Format("metric not found: {0}", metricId));
                }
                DateTime now = DateTime.Parse(utcNow);
                if (pageIndex == 0 && !force)
                {
                    var query = _metricSource(metric.DataSource.DataSourceTypeId).QueryInfo(metric, now);

                    var min = query.QueryStartDate;
                    var max = query.QueryEndDate;

                    var mdp = db.MetricDataPoints.Where(e => e.MetricId == metricId && e.QueryStartDate <= min && e.QueryEndDate >= max).FirstOrDefault();
                    if (mdp != null && mdp.Id != metricDatapointId.GetValueOrDefault())
                    {
                        _logger.Warn("metric already run for specified date");

                        _logger.Warn("datapoint id:{0}, query date:{2}-{3}, execution date:{1}", mdp.Id, mdp.CreateDateUTC, mdp.QueryStartDate, mdp.QueryEndDate);

                        if (!force)
                            throw new ArgumentException("metric already executed for specified date. include force=true option to re-run (may result in redundant data).");
                    }
                }
            }
            
            /// enque the metric with a specific datetime so that any retries can generate the same query
            _queue.Enqueue<MetricTaskRunner>(e => e.ExecuteMetric(request));


        }


        [DisplayName("Download Analytics Metric")]
        public void ExecuteMetric(ExecuteMetricTaskRequest request)
        {
            int metricId = request.MetricId;
            string utcNow = request.UtcNow;
            int pageIndex = request.PageIndex;
            int? metricDatapointId = request.MetricDatapointId;

            Metric metric;
            using (var db = new EF.AnalyticsDbContext())
            {
                metric = db.Metrics.Include("DataSource")
                    .Include("DataDestination")
                    .FirstOrDefault(e => e.Id == metricId);


                if (metric == null)
                {
                    throw new ArgumentOutOfRangeException("metricId", string.Format("metric id not found: {0}", metricId));
                }
                


                var runner = _metricSource(metric.DataSource.DataSourceTypeId);
                var result = runner.Retrieve(metric, DateTime.Parse(utcNow),pageIndex);
                result.MetricId = metric.Id;
                result.CreateDateUTC = DateTime.UtcNow;
                if (metricDatapointId.HasValue)
                {
                    result.Id = metricDatapointId.Value;
                }
                var destination = _metricDestination(metric.DataDestination.DataSourceTypeId);
                var task = destination.SaveAsync(
                    metric,
                    result,
                    result.Data
                );

                

                Task.WaitAll(task);

                metricDatapointId = result.Id;

                if (result.More)
                {
                    //queue up the next page !!
                    EnqueueMetric(metricId, utcNow, pageIndex + 1,metricDatapointId,true);
                }

            }

        }

    }
}
