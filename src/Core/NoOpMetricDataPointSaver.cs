﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace csgp.CentralizedAnalytics
{
    /// <summary>
    /// data point saver that does nothing with the datapoints
    /// (it logs them)
    /// </summary>
    public class NoOpMetricDataPointSaver : IMetricDataPointSaver
    {
        async Task IMetricDataPointSaver.SaveAsync(Metric metric, MetricDataPoint datapoint,MetricData data)
        {
            Utility.Logger.Info("NoOp DataPoint Saver");
            Utility.Logger.Info(JsonConvert.SerializeObject(metric));
            Utility.Logger.Info(JsonConvert.SerializeObject(datapoint));

            await Task.Yield();
            
        }
    }
}
