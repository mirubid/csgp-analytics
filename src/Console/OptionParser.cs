﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Options;


namespace csgp.CentralizedAnalytics
{
    class OptionParser :ICmdConfigurator
    {
        private OptionSet _options = null;
        string _description = null;
        public OptionParser()
        {
            _options = new OptionSet();


        }
        public List<string> Parse(IEnumerable<string> args)
        {
            return _options.Parse(args);
        }


        public ICmdConfigurator AddOption(string prototype,string description,Action<string> action)
        {

            _options.Add(prototype, description, action);
            return this; // fluent chaining
        }

        public void WriteHelp(System.IO.TextWriter txtWriter){
            if (txtWriter == null)
            {
                txtWriter = Console.Error;
            }
            if (!string.IsNullOrEmpty(_description))
            {
                txtWriter.WriteLine(_description);
            }
            _options.WriteOptionDescriptions(txtWriter);
            txtWriter.Flush();
        }


        ICmdConfigurator ICmdConfigurator.SetDescription(string description)
        {
            _description = description;
            return this;
        }
    }
}