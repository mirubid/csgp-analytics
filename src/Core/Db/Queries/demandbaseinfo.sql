use MarketPlaceEcomSystemAnalytic

-- view query definitions related to demandbase reports --
select * from dbo.Metric where name like 'demandbase%'

-- view query execution history for demandbase-page metric --
SELECT  QueryStartDate
  FROM [dbo].[MetricDataPoint]
  where MetricId=13
  group by QueryStartDate

-- count records for demandbase-page
select count(*) from dbo.MetricData_13_View

-- select top n demandbase records
select count(*) from dbo.DemandbaseCompany
select top 100 * from dbo.DemandbaseCompany order by DemandBaseSID desc

;with performance (day,duration) as (

select CAST(j.CreatedAt As Date),  CAST(JSON_VALUE(Data,'$.PerformanceDuration') as int) [PerformanceDuration] from Hangfire.Job j

join Hangfire.State s
	on j.StateId=s.Id

 where j.StateName='Succeeded'
)
select [day], Sum(duration) [Total(ms)], Avg(duration) [Average(ms)], count(*) Count from performance
group by [day]

