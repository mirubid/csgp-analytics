﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace csgp.CentralizedAnalytics.GoogleDataSource
{
    public class DataSourceConfig
    {
        [JsonProperty("authStrategy")]
        public string AuthStrategy { get; set; }
        [JsonProperty("accountId")]        
        public string AccountId { get; set; }
    }

    public class RequestConfig
    {
        //
        // Summary:
        //     A comma-separated list of Analytics dimensions. E.g., 'ga:browser,ga:city'.
        [JsonProperty("dimensions")]
        public virtual string Dimensions { get; set; }
        //
        // Summary:
        //     End date for fetching Analytics data. Request can should specify an end date
        //     formatted as YYYY-MM-DD, or as a relative date (e.g., today, yesterday, or 7daysAgo).
        //     The default value is yesterday.
        [JsonProperty("end-date")]
        public virtual string EndDate { get; set; }
        //
        // Summary:
        //     A comma-separated list of dimension or metric filters to be applied to Analytics
        //     data.
        [JsonProperty("filters")]
        public virtual string Filters { get; set; }

        //
        // Summary:
        //     Unique table ID for retrieving Analytics data. Table ID is of the form ga:XXXX,
        //     where XXXX is the Analytics view (profile) ID.
        [JsonProperty("ids")]
        public virtual string Ids { get; set; }
        //
        // Summary:
        //     The response will include empty rows if this parameter is set to true, the default
        //     is true
        [JsonProperty("include-empty-rows")]
        public virtual bool? IncludeEmptyRows { get; set; }
        //
        // Summary:
        //     The maximum number of entries to include in this feed.
        [JsonProperty("max-results")]
        public virtual int? MaxResults { get; set; }

        //
        // Summary:
        //     A comma-separated list of Analytics metrics. E.g., 'ga:sessions,ga:pageviews'.
        //     At least one metric must be specified.
        [JsonProperty("metrics")]
        public virtual string Metrics { get; set; }

        //
        // Summary:
        //     The desired sampling level.
        [JsonProperty("samplingLevel")]
        public virtual string SamplingLevel { get; set; }
        //
        // Summary:
        //     An Analytics segment to be applied to data.
        [JsonProperty("segment")]
        public virtual string Segment { get; set; }
        //
        // Summary:
        //     A comma-separated list of dimensions or metrics that determine the sort order
        //     for Analytics data.
        [JsonProperty("sort")]
        public virtual string Sort { get; set; }
        //
        // Summary:
        //     Start date for fetching Analytics data. Requests can specify a start date formatted
        //     as YYYY-MM-DD, or as a relative date (e.g., today, yesterday, or 7daysAgo). The
        //     default value is 7daysAgo.
        [JsonProperty("start-date")]
        public virtual string StartDate { get; set; }
        //
        // Summary:
        //     An index of the first entity to retrieve. Use this parameter as a pagination
        //     mechanism along with the max-results parameter.
        [JsonProperty("start-index")]
        public virtual int? StartIndex { get; set; }

        public static RequestConfig FromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return new RequestConfig();
            
            var query = new Uri(url).Query;
            if (query.StartsWith("?"))
            {
                query = query.Substring(1);
            }
            return
                query.Split('&')
                .Select(e => e.Split('='))
                .Where(e => e.Length == 2)
                .Aggregate(
                    new Newtonsoft.Json.Linq.JObject(),
                    (seed, e) => {
                        seed[e[0]] = Uri.UnescapeDataString(e[1]);
                        return seed;
                    })
                .ToObject<RequestConfig>();
                
        }
    }
}
