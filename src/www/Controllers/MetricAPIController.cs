﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
    public class MetricController : ApiController
    {
        private csgp.CentralizedAnalytics.EF.AnalyticsDbContext db = new csgp.CentralizedAnalytics.EF.AnalyticsDbContext();
        private MetricTaskScheduler _scheduler;

        public MetricController(EF.AnalyticsDbContext dbContext,MetricTaskScheduler scheduler)
        {
            this.db = dbContext;
            _scheduler = scheduler;
        }
        public IQueryable<MetricDTO> GetMetrics(int pageSize = 10
                        ,System.String TenantId = null
                        ,System.String DataSourceId = null
                        ,System.String DataDestinationId = null
                )
        {
            var model = db.Metrics.AsQueryable();
                                if(TenantId != null){
                        model = model.Where(m=> m.TenantId == TenantId);
                    }
                                if(DataSourceId != null){
                        model = model.Where(m=> m.DataSourceId == DataSourceId);
                    }
                                if(DataDestinationId != null){
                        model = model.Where(m=> m.DataDestinationId == DataDestinationId);
                    }
                        
            return model.Select(MetricDTO.SELECT).Take(pageSize);
        }

        [ResponseType(typeof(MetricDTO))]
        public async Task<IHttpActionResult> GetMetric(int id)
        {
            var model = await db.Metrics.Select(MetricDTO.SELECT).FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        public async Task<IHttpActionResult> PutMetric(int id, Metric model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MetricExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            _scheduler.ScheduleMetric(model);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(MetricDTO))]
        public async Task<IHttpActionResult> PostMetric(Metric model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Metrics.Add(model);
            await db.SaveChangesAsync();
            var ret = await db.Metrics.Select(MetricDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            _scheduler.ScheduleMetric(model);
            return CreatedAtRoute("DefaultApi", new { id = model.Id }, model);
        }

        [ResponseType(typeof(MetricDTO))]
        public async Task<IHttpActionResult> DeleteMetric(int id)
        {
            Metric model = await db.Metrics.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.Metrics.Remove(model);
            await db.SaveChangesAsync();
            _scheduler.RemoveScheduledMetric(model);
            var ret = await db.Metrics.Select(MetricDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return Ok(ret);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MetricExists(int id)
        {
            return db.Metrics.Count(e => e.Id == id) > 0;
        }
    }
}