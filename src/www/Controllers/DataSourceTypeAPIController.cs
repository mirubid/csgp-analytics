﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
    public class DataSourceTypeController : ApiController
    {
        private csgp.CentralizedAnalytics.EF.AnalyticsDbContext db = new csgp.CentralizedAnalytics.EF.AnalyticsDbContext();

        public IQueryable<DataSourceTypeDTO> GetDataSourceTypes(int pageSize = 10
                )
        {
            var model = db.DataSourceTypes.AsQueryable();
                        
            return model.Select(DataSourceTypeDTO.SELECT).Take(pageSize);
        }

        [ResponseType(typeof(DataSourceTypeDTO))]
        public async Task<IHttpActionResult> GetDataSourceType(string id)
        {
            var model = await db.DataSourceTypes.Select(DataSourceTypeDTO.SELECT).FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        public async Task<IHttpActionResult> PutDataSourceType(string id, DataSourceType model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DataSourceTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(DataSourceTypeDTO))]
        public async Task<IHttpActionResult> PostDataSourceType(DataSourceType model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DataSourceTypes.Add(model);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DataSourceTypeExists(model.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            var ret = await db.DataSourceTypes.Select(DataSourceTypeDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return CreatedAtRoute("DefaultApi", new { id = model.Id }, model);
        }

        [ResponseType(typeof(DataSourceTypeDTO))]
        public async Task<IHttpActionResult> DeleteDataSourceType(string id)
        {
            DataSourceType model = await db.DataSourceTypes.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.DataSourceTypes.Remove(model);
            await db.SaveChangesAsync();
            var ret = await db.DataSourceTypes.Select(DataSourceTypeDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return Ok(ret);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DataSourceTypeExists(string id)
        {
            return db.DataSourceTypes.Count(e => e.Id == id) > 0;
        }
    }
}