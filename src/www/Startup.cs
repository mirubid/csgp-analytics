﻿using Microsoft.Owin;
using Owin;
using Hangfire;

[assembly: OwinStartupAttribute(typeof(csgp.CentralizedAnalyticsProcessing.Startup))]
namespace csgp.CentralizedAnalyticsProcessing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            ConfigureAuth(app);
            ConfigureHangfire(app);
        }
    }
}
