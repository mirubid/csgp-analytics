﻿(function (ng) {

    ng.module('Tenant')
    .controller('Tenant_TenantDataSource_list', ['$scope', '$http', '$location', function ($scope, $http, $location) {

        $scope.__name = 'Tenant_TenantDataSource_list';
        $scope.data = [];
        $scope.dataSourceTypes = [];
        $http.get('/Api/DataSourceType/')
            .then(function (response) { $scope.dataSourceTypes = response.data; });

        function init(tenant) {
            if (!tenant) return;
            $scope.tenant = tenant;
            $http.get('/Api/TenantDataSource/', { params: { TenantId: tenant.Id } })
            .then(function (response) { $scope.data = response.data; });
        }

        $scope.$parent.$watch('data', function (newValue, oldValue, scope) {
            console.log('scope.watch', arguments);
            init(newValue);
        });

        $scope.createTenantDataSource = function (id) {
            console.log('createTenantDataSource', id);
            $scope.dataSourceTypeId = id;

        };
        $scope.displayMetrics = function (id,tenantId) {
            $scope.$emit('TenantDataSource_Metrics::Show', { DataSourceId: id, TenantId: tenantId });
        }
        $scope.createMetric = function (id, tenantId, dataSourceTypeId) {
            $scope.$emit('TenantDataSource_Metrics::Create', { DataSourceId: id, TenantId: tenantId, DataSourceTypeId: dataSourceTypeId });
        }
        $scope.$on('TenantDataSource_edit::Cancel', function () { $scope.dataSourceTypeId = null; });
        $scope.$on('TenantDataSource_edit::Saved', function () {
            $scope.dataSourceTypeId = null;
            init($scope.tenant);
        });
        
    }]);
})(angular);