﻿using Google.Apis.Analytics.v3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using static Google.Apis.Analytics.v3.Data.GaData;

namespace csgp.CentralizedAnalytics.GoogleDataSource
{
    public class GADataRetriever:IMetricRetriever
    {
        private static ILogger _log = NLog.LogManager.GetCurrentClassLogger();
        internal const int MaxResults = 10000;
        public MetricDataPoint RetrieveData(Metric metric,DateTime? now=null,int pageIndex=0)
        {

            var ga_query_config = GetQueryConfig(metric, now, pageIndex);

            var init = new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = GoogleApiHelper.ServiceCredential,

                ApplicationName = "csgp.CentralizedAnalytics",
                ApiKey =GoogleApiHelper.ApiKey //" AIzaSyCdsV7TUb9UqZsgX06QSSHSzl5hNhDM5oE "// " AIzaSyA6rZhswdOwBREU7a2N4MFe92 -ZIrYW62c",

            };

            AnalyticsService service = new AnalyticsService(init);

            var startdate = ga_query_config.StartDate.ParseAsDate(now.Value);
            var enddate = ga_query_config.EndDate.ParseAsDate(now.Value);

            var req = service.Data.Ga.Get(
                ids: ga_query_config.Ids,
                startDate: startdate.ToYMD(),
                endDate: enddate.ToYMD(),
                metrics: ga_query_config.Metrics);

            if (!string.IsNullOrEmpty(ga_query_config.Dimensions)) req.Dimensions = ga_query_config.Dimensions;
            if (!string.IsNullOrEmpty(ga_query_config.Filters)) req.Filters = ga_query_config.Filters;
            if (ga_query_config.IncludeEmptyRows != null) req.IncludeEmptyRows = ga_query_config.IncludeEmptyRows;
            if (ga_query_config.MaxResults != null) req.MaxResults = ga_query_config.MaxResults;
            if (ga_query_config.SamplingLevel != null)
            {
                DataResource.GaResource.GetRequest.SamplingLevelEnum sle;
                if (Enum.TryParse<DataResource.GaResource.GetRequest.SamplingLevelEnum>(ga_query_config.SamplingLevel, true, out sle))
                {
                    req.SamplingLevel = sle;
                }
            }
            if (!string.IsNullOrEmpty(ga_query_config.Segment)) req.Segment = ga_query_config.Segment;
            if (!string.IsNullOrEmpty(ga_query_config.Sort)) req.Sort = ga_query_config.Sort;
            if (ga_query_config.StartIndex != null) req.StartIndex = ga_query_config.StartIndex;

            if(req.MaxResults!=null && req.MaxResults > MaxResults)
            {
                req.MaxResults = MaxResults;
            }
            if (req.MaxResults != null)
            {
                req.StartIndex = 1 + (pageIndex * req.MaxResults);
            }
            var data = req.Execute();

            //_log.Info(JsonConvert.SerializeObject(data));
            var result = new MetricData()
            {
                Dimensions = data.ColumnHeaders.Where(e => e.ColumnType == "DIMENSION").Select(e => e.Name).ToList(),
                Metrics = data.ColumnHeaders.Where(e => e.ColumnType == "METRIC").Select(e => e.Name).ToList(),
                Rows = new List<MetricDataRow>()
            };
            int col_count = result.Dimensions.Count + result.Metrics.Count;

            if (data.Rows!=null && data.Rows.Count > 0)
            {
                result.Rows = data.Rows.Select(e =>
                {
                    var mdr = new MetricDataRow()
                    {
                        Dimensions = new List<string>(),
                        Metrics = new List<double>()
                    };
                    double value;
                    for (int i = 0; i < data.ColumnHeaders.Count; i++)
                    {
                        switch (data.ColumnHeaders[i].ColumnType)
                        {
                            case "DIMENSION":
                                mdr.Dimensions.Add(e[i]);
                                break;
                            case "METRIC":
                                if (!double.TryParse(e[i], out value))
                                {
                                    value = 0;
                                }
                                mdr.Metrics.Add(value);
                                break;
                            default:
                                _log.Warn("unknown column type {0}", data.ColumnHeaders[i].ColumnType);
                                break;
                        }
                    }
                    return mdr;
                }).ToList();

               
            }
            return new MetricDataPoint() {
                QueryStartDate = startdate,
                QueryEndDate = enddate,
                Value = 0,
                PageIndex = pageIndex,
                More = (result.Rows!=null && result.Rows.Count == req.MaxResults),
                Data = result
            };
        }
        public MetricQuery QueryInfo(Metric metric, DateTime? now = null, int pageIndex = 0)
        {
            var ga_query_config = GetQueryConfig(metric, now, pageIndex);
            var startdate = ga_query_config.StartDate.ParseAsDate(now.Value);
            var enddate = ga_query_config.EndDate.ParseAsDate(now.Value);

            return new MetricQuery()
            {
                QueryStartDate = startdate,
                QueryEndDate = enddate
            };
        }
        private RequestConfig GetQueryConfig(Metric metric, DateTime? now = null, int pageIndex = 0)
        {
            if (metric == null)
            {
                throw new ArgumentNullException("metric");
            }
            if (metric.DataSource == null)
            {
                throw new ArgumentException("metric.DataSource is null", "metric");
            }
            if (metric.DataSource.DataSourceTypeId != "GA")
            {
                throw new InvalidOperationException("invalid DataSourceTypeId");
            }
            if (now == null)
            {
                now = DateTime.Now;
            }
            var ga_ds_config = JsonConvert.DeserializeObject<DataSourceConfig>(metric.DataSource.Configuration);
            var ga_query_config =
                metric.Configuration.StartsWith("http") ?
                      RequestConfig.FromUrl(metric.Configuration)
                    : JsonConvert.DeserializeObject<RequestConfig>(metric.Configuration);

            if (ga_query_config.MaxResults == null)
            {
                ga_query_config.MaxResults = MaxResults;
            }
            return ga_query_config;
        }
        MetricDataPoint IMetricRetriever.Retrieve(Metric metric,DateTime utcNow, int pageIndex)
        {
            return RetrieveData(metric, utcNow, pageIndex);
        }
    }
}
