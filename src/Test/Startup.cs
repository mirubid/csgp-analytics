﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Hangfire;
using Hangfire.SqlServer;
using System.Diagnostics;

namespace csgp.CentralizedAnalytics
{
    [TestClass]
    public class Startup
    {
        [AssemblyInitialize]
        public static void Initialize(TestContext context)
        {
            try {
                GlobalConfiguration.Configuration.UseSqlServerStorage("hangfire");
                Hangfire.Common.JobHelper.SetSerializerSettings(new Newtonsoft.Json.JsonSerializerSettings()
                {
                    DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind
                });
            }catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }
    }
}
