﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public class MetricDataPoint
    {
        public MetricDataPoint()
        {
            CreateDateUTC = DateTime.UtcNow;
        }
        public int Id { get; set; }
        [Required]
        public int MetricId{get;set;}
        protected Metric Metric { get; set; }

        public double Value { get; set; }
        public DateTime CreateDateUTC { get; set; }

        public DateTime QueryStartDate { get; set; }
        public DateTime QueryEndDate { get; set; }


        public MetricData Data { get; set; }

        /// <summary>
        /// 0 based index used to indicate subsequent pages for the same datapoint
        /// 0 is the first page
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// indication of whether more records available and thus the next page should be scheduled
        /// </summary>
        public bool More { get; set; }
    }

}
