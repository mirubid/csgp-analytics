﻿(function () {
    'use strict';
    

    angular.module('TenantDataSource', ['ngRoute'])
    .controller('TenantDataSource_edit_GA', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location) {
        console.log('TenantDataSource_edit_GA', $scope);


        $scope.__mode = 'create';
        $scope.data = {
            DataSourceTypeId:'GA',
            Id: '',
            ApiKey: '',
            UserId: '',
            ClientSecret: '',
            config:{authStrategy:'service-account',accountId:null}
        };

        function initTenant(tenant) {
            $scope.data.TenantId = tenant.Id;
        }

        $scope.$parent.$watch('dataSourceTypeId', function () { console.log('TenantDataSource_edit_GA watch', arguments); });
        $scope.$parent.$watch('tenant', function (newv, oldv) { initTenant(newv); });


        $scope.save = function () {

            var data = {
                Id: $scope.data.Id,
                TenantId: $scope.data.TenantId,
                DataSourceTypeId: $scope.data.DataSourceTypeId,
                Configuration: JSON.stringify($scope.data.config)
            };
            var req;
            if ($scope.__mode === 'create') {
                req = $http.post('/Api/TenantDataSource', data);
            } else {
                req = $http.put('/Api/TenantDataSource/' + data.id, data);
            }
            req.then(function (response) {
                console.log('saved TenantDataSource', response);
                $scope.$emit('TenantDataSource_edit::Saved', data);
            });
        };

        $scope.cancel = function () {

            $scope.$emit('TenantDataSource_edit::Cancel', {});
        };

        if ($scope.$parent.data) {
            initTenant($scope.$parent.data);
        }

    }])
    ;

})();
