﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.EF
{
    public class AnalyticsDbContext : DbContext
    {
        public AnalyticsDbContext() : base("CentralizedAnalyticsDb")
        {

        }

        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<TenantDataSource> TenantDataSources { get; set; }

        public DbSet<Metric> Metrics { get; set; }

        public DbSet<MetricDataPoint> MetricDataPoints{get;set;}

        public DbSet<DataSourceType> DataSourceTypes{get; set;}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<TenantDataSource>()
                .HasKey(e => new { e.TenantId, e.Id });

            modelBuilder.Entity<Tenant>().HasMany<Metric>(e => e.Metrics)
                .WithRequired()
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Metric>().HasRequired<TenantDataSource>(e => e.DataSource)
                .WithMany(e => e.Metrics)
                .HasForeignKey(e=>new { e.TenantId, e.DataSourceId })
                .WillCascadeOnDelete(false)
                ;

            modelBuilder.Entity<Metric>().HasRequired<TenantDataSource>(e => e.DataDestination)
                .WithMany()
                .HasForeignKey(e => new { e.TenantId, e.DataDestinationId })
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<MetricDataPoint>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<MetricDataPoint>()
                .Ignore(e => e.Data)
                .Ignore(e=>e.More);

            
            base.OnModelCreating(modelBuilder);
        }
        
    }
}
