﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace csgp.CentralizedAnalytics
{
    class Program
    {
        
        static void Main(string[] args)
        {

            Environment.ExitCode = new ConsoleContext().Run(args);

        }

    }
}
