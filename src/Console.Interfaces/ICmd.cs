﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public interface ICmd
    {
        /// <summary>
        /// register any commandline syntax 
        /// </summary>
        /// <param name="options"></param>
        void ConfigureOptions(ICmdConfigurator options);
        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="extraArgs">extra arguments not handled by options in the command line</param>
        /// <returns>exit code</returns>
        int Execute(List<string> extraArgs);

        string[] Names { get; }
    }
    
}
