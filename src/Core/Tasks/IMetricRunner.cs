﻿using System;
using System.Collections.Generic;

namespace csgp.CentralizedAnalytics
{
    public interface IMetricRetriever
    {

        /// <summary>
        /// retrieve a datapoint for a specific query
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="now"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        MetricDataPoint Retrieve(Metric metric,DateTime now,int pageIndex=0);

        /// <summary>
        /// retrieve some info about the query generated for the specified endpoint.
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="now"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        MetricQuery QueryInfo(Metric metric, DateTime? now = null, int pageIndex = 0);
    }
}