﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Google.Apis.Analytics.v3;
using System.Diagnostics;
using Google.Apis.Auth.OAuth2;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Analytics.v3.Data;
using System.Linq;
using Newtonsoft.Json;

namespace csgp.CentralizedAnalytics
{
    
    [TestClass]
    public class GoogleAnalyticsTests
    {
        private GoogleDataSource.DataSourceConfig _config;

        public GoogleAnalyticsTests()
        {
            _config = new GoogleDataSource.DataSourceConfig();


            

        }
        const string ga_id = "service-account@milby.iam.gserviceaccount.com";// "cityfeet-service-account@api-project-633950454552.iam.gserviceaccount.com";
        //https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A93381362&start-date={start-date}&end-date={end-date}&metrics=ga%3Asessions&access_token={access_token}
        [TestMethod]
        public async Task ShouldBeAbleToQueryGoogleAnalytics()
        {

            // query explorer https://ga-dev-tools.appspot.com/query-explorer/

            
            var init = new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = GoogleApiHelper.GetServiceAccountCredential(),
                ApplicationName="csgp.CentralizedAnalytics",
                ApiKey = GoogleApiHelper.ApiKey //" AIzaSyCdsV7TUb9UqZsgX06QSSHSzl5hNhDM5oE "// " AIzaSyA6rZhswdOwBREU7a2N4MFe92 -ZIrYW62c",
                
            };
            AnalyticsService service = new AnalyticsService(init);
            Accounts account_list=null;
            try
            {
                var account_list_request = service.Management.Accounts.List();
                account_list = await account_list_request.ExecuteAsync();
                Assert.IsNotNull(account_list, "account_list");

                Debug.WriteLine("accounts");
                Debug.WriteLine(string.Join(",", account_list.Items.Select(e => string.Format("{0}:{1}", e.Id, e.Name))));
            }catch(Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            var account = account_list.Items.First(e => e.Name == "LoopNet");
            string account_id = account.Id;
            Debug.WriteLine("{0} {1} {2}", account.Id, account.Name, account.Kind);
            var web_properties = await service.Management.Webproperties.List(account_id)
                .ExecuteAsync();
            Debug.WriteLine("profiles");
            Debug.WriteLine(
                string.Join(",", web_properties.Items.Select(e => string.Format("{0} {1} {2}",e.AccountId,e.Id,e.Name,e.ProfileCount)))
            );
            var property = web_properties.Items.First(e=>e.Name=="www.loopnet.com - QA");

            var profiles= await service.Management.Profiles.List(account.Id, property.Id)
                .ExecuteAsync();

            Assert.IsNotNull(profiles, "profiles");
            Debug.WriteLine("profiles for {0}/{1} - {2}", property.AccountId,property.Id, property.Name);
            profiles.Items.ToList().ForEach(e =>
            {
                Debug.WriteLine("{0}/{1}/{2} - {3}", e.AccountId, e.WebPropertyId, e.Id, e.Name);
            });
            var profile = profiles.Items.First();

            

            var req = service.Data.Ga.Get(                
                ids: "ga:" + profile.Id, /* "ga:90146724"*/ /* "ga:93381362"*/ 
                startDate:"8daysAgo", 
                endDate: "yesterday" , 
                metrics: "ga:sessions");
            
            
            Debug.WriteLine(req.ToString());
            Debug.WriteLine(req.RestPath);


            var data = await req.ExecuteAsync();
            Assert.IsNotNull(data, "data");

            Debug.WriteLine(data.TotalResults);

            Debug.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(data));

        }
        [TestMethod]
        public async Task ShouldBeAbleToMakeAGenericUrlRequest()
        {


            var serviceAccount = GoogleApiHelper.GetServiceAccountCredential(ga_id, Properties.Resources.CityfeetTestUserCredential);

            var rb = new Google.Apis.Requests.RequestBuilder();
            
            var token = await serviceAccount.GetAccessTokenForRequestAsync();
            string url = "https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A90146724&start-date=yesterday&end-date=yesterday&metrics=ga%3AbounceRate%2Cga%3Apageviews"
                + "&access_token=" + token;
            
            Debug.WriteLine(token);
           

            Debug.WriteLine(url);

            var resp = await new System.Net.Http.HttpClient().GetStringAsync(url);

            Debug.WriteLine(resp);

            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<GaData>(resp);

            if (data.ColumnHeaders != null)
            {
                foreach(var h in data.ColumnHeaders)
                {
                    Debug.Write(h.DataType);
                    Debug.WriteLine(h.Name);
                }
            }
            if (data.Rows != null)
            {
                foreach(var row in data.Rows)
                {
                    foreach(var cell in row)
                    {
                        Debug.WriteLine(cell);
                    }
                }
            }
            if (data.DataTable!=null && data.DataTable.Rows.Count > 0)
            {
                var cols = data.DataTable.Rows[0].C;
                foreach(var col in cols)
                {
                    Debug.WriteLine(col.V);
                }
            }
        }
        [TestMethod]
        public void ShouldBeAbleToCreateAServiceCredential()
        {
            ServiceAccountCredential credential = GoogleApiHelper.GetServiceAccountCredential(
                  ga_id
                , Properties.Resources.CityfeetTestUserCredential 
                );

            Assert.IsNotNull(credential);
            Assert.AreEqual(credential.Id, ga_id);

        }
        [TestMethod]
        public void ShouldBeAbleToParseAQueryUrl()
        {
            string url = "https://ga-dev-tools.appspot.com/query-explorer/?ids=ga%3A90146724&start-date=yesterday&end-date=yesterday&metrics=ga%3AbounceRate%2Cga%3Apageviews&dimensions=ga%3AsessionCount&sort=ga%3AbounceRate&segment=gaid%3A%3A-5&start-index=1&max-results=5000&filters=ga:browser%3D~%5EFirefox&samplingLevel=DEFAULT";

            var req=GoogleDataSource.RequestConfig.FromUrl(url);

            Assert.IsNotNull(req, "req");

            Assert.AreEqual("yesterday", req.StartDate, @" ""yesterday"" vs actual");

            Assert.AreEqual("yesterday", req.EndDate, @" ""yesterday"" vs req.EndDate");

            Assert.AreEqual("ga:90146724", req.Ids, @" ""ga:90146724"" vs req.Ids");

            Debug.WriteLine(JsonConvert.SerializeObject(req));
        }
        [TestMethod]
        public void ShouldBeAbleToCreateAMetricBasedOnAQueryUrl()
        {
            string url = "https://ga-dev-tools.appspot.com/query-explorer/?ids=ga%3A90146724&start-date=yesterday&end-date=yesterday&metrics=ga%3AbounceRate%2Cga%3Apageviews&dimensions=ga%3AsessionCount&sort=ga%3AbounceRate&segment=gaid%3A%3A-5&start-index=1&max-results=5000&filters=ga:browser%3D~%5EFirefox&samplingLevel=DEFAULT";

            var req = GoogleDataSource.RequestConfig.FromUrl(url);
            int metric_id;
            using (var db = new EF.AnalyticsDbContext())
            {

                var tenant = db.Tenants.Include("DataSources").FirstOrDefault(e=>e.Id=="Test");

                Assert.IsNotNull(tenant, "tenant");
                tenant.DataSources.AssertAny("DataSources");

                var metric = new Metric()
                {          
                    TenantId=tenant.Id,          
                    Name = "test metric for ga",
                    Configuration = JsonConvert.SerializeObject(req),                    
                    DataDestinationId="NULL Test",
                    Description = "A test metric"
                    
                };
                tenant.DataSources.First(e => e.DataSourceTypeId == "GA")
                    .Metrics.Add(metric);
                try
                {
                    db.SaveChanges();
                }catch (System.Data.Entity.Validation.DbEntityValidationException ex){


                    ex.EntityValidationErrors.SelectMany(e => e.ValidationErrors)
                        .Select(e => string.Format("{0} {1}", e.PropertyName, e.ErrorMessage))
                        .ToList()
                        .ForEach(e=>Debug.WriteLine(e));
                    throw;
                }

                Assert.AreNotEqual(0, metric.Id, @" 0 vs metric.Id");
                metric_id = metric.Id;
            }
            using(var db = new EF.AnalyticsDbContext())
            {
                var metric = db.Metrics.FirstOrDefault(e => e.Id == metric_id);
                Assert.IsNotNull(metric, "metric");

            }
        }
        [TestMethod]
        public void ShouldBeAbleToExecuteGAQueryFromGAMetric()
        {
            
            Metric metric;
            using(var db=new EF.AnalyticsDbContext())
            {
                var ds = db.TenantDataSources.Include("Metrics").FirstOrDefault(e => e.TenantId == "Test" && e.Id == "GA Test");
                Assert.IsNotNull(ds, "ds");
                Assert.IsNotNull(ds.Metrics, "ds.Metrics");
                Assert.AreNotEqual(0, ds.Metrics.Count, @" 0 vs ds.Metrics.Count");

                metric = ds.Metrics.FirstOrDefault(e => e.Name == "GA Test Metric");

                Assert.IsNotNull(metric, "metric");

                Assert.IsNotNull(metric.DataSource, "metric.DataSource");


                

            }

            var ga_ds_config = JsonConvert.DeserializeObject<GoogleDataSource.DataSourceConfig>(metric.DataSource.Configuration);
            var ga_query_config = JsonConvert.DeserializeObject<GoogleDataSource.RequestConfig>(metric.Configuration);

            Assert.IsNotNull(ga_ds_config, "ga_ds_config");

            var init = new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = GoogleApiHelper.GetServiceAccountCredential(),
                ApplicationName = "csgp.CentralizedAnalytics",
                ApiKey = GoogleApiHelper.ApiKey //" AIzaSyCdsV7TUb9UqZsgX06QSSHSzl5hNhDM5oE "// " AIzaSyA6rZhswdOwBREU7a2N4MFe92 -ZIrYW62c",

            };
            AnalyticsService service = new AnalyticsService(init);



            var req = service.Data.Ga.Get(
                ids: ga_query_config.Ids,
                startDate: ga_query_config.StartDate,
                endDate: ga_query_config.EndDate, 
                metrics: ga_query_config.Metrics);

            if (!string.IsNullOrEmpty(ga_query_config.Dimensions)) req.Dimensions = ga_query_config.Dimensions;
            if (!string.IsNullOrEmpty(ga_query_config.Filters)) req.Filters = ga_query_config.Filters;
            if (ga_query_config.IncludeEmptyRows!=null) req.IncludeEmptyRows = ga_query_config.IncludeEmptyRows;
            if (ga_query_config.MaxResults != null) req.MaxResults = ga_query_config.MaxResults;
            if (ga_query_config.SamplingLevel != null) {
                DataResource.GaResource.GetRequest.SamplingLevelEnum sle;
                if(Enum.TryParse<DataResource.GaResource.GetRequest.SamplingLevelEnum>(ga_query_config.SamplingLevel, true, out sle))
                {
                    req.SamplingLevel = sle;
                }
            }
            if (!string.IsNullOrEmpty(ga_query_config.Segment)) req.Segment = ga_query_config.Segment;
            if (!string.IsNullOrEmpty(ga_query_config.Sort)) req.Sort = ga_query_config.Sort;
            if (ga_query_config.StartIndex != null) req.StartIndex = ga_query_config.StartIndex;



            Debug.WriteLine(req.ToString());
            Debug.WriteLine(req.RestPath);


            var data = req.Execute();
            Assert.IsNotNull(data, "data");

            Debug.WriteLine(data.TotalResults);

            Debug.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(data));

            
        }
        [TestMethod]
        public void ShouldBeAbleToScheduleARecurringMetricJob()
        {
            Metric metric;
            using (var db = new EF.AnalyticsDbContext())
            {
                var ds = db.TenantDataSources.Include("Metrics").FirstOrDefault(e => e.TenantId == "Test" && e.Id == "GA Test");
                Assert.IsNotNull(ds, "ds");
                Assert.IsNotNull(ds.Metrics, "ds.Metrics");
                Assert.AreNotEqual(0, ds.Metrics.Count, @" 0 vs ds.Metrics.Count");

                metric = ds.Metrics.FirstOrDefault(e => e.Name == "GA Test Metric");

                Assert.IsNotNull(metric, "metric");

                Assert.IsNotNull(metric.DataSource, "metric.DataSource");




            }
            
            Hangfire.RecurringJob.AddOrUpdate< Tasks.MetricTaskRunner>(
                string.Format("metric job:{0}", metric.Id), 
                (runner) => runner.EnqueueMetric(metric.Id), Hangfire.Cron.Daily);
        }
        [TestMethod]
        public async Task ShouldBeAbleToGetTheTimezone()
        {
            var service = GetAnalyticsService();

            var accounts=await service.Management.Accounts.List().ExecuteAsync();

            Assert.IsNotNull(accounts, "accounts");

            Debug.WriteLine(JsonConvert.SerializeObject(accounts));
            
        }
        [TestMethod]
        public async Task ShouldbeAbleToDownloadComplexQuery()
        {
            // Properties.Resources.GA_Query_01
            Metric metric;
            using (var db = new EF.AnalyticsDbContext())
            {
                var ds = db.TenantDataSources.Include("Metrics").FirstOrDefault(e => e.TenantId == "Test" && e.Id == "GA Test");
                Assert.IsNotNull(ds, "ds");
                Assert.IsNotNull(ds.Metrics, "ds.Metrics");
                Assert.AreNotEqual(0, ds.Metrics.Count, @" 0 vs ds.Metrics.Count");

                metric = ds.Metrics.FirstOrDefault(e => e.Name == "GA Complex Test Metric");
                if (metric == null)
                {
                    metric = new Metric()
                    {
                        Configuration = Properties.Resources.GA_Query_01,
                        DataDestinationId = "LocalDb Test",
                        DataSourceId = ds.Id,
                        Name = "GA Complex Test Metric",
                        Description = "GA Complex Test Metric",
                        Schedule = "0 8 * * *",
                        TenantId = ds.TenantId,

                    };
                    db.Metrics.Add(metric);
                    await db.SaveChangesAsync();
                }
                Assert.IsNotNull(metric, "metric");

                




            }

            var dp = new GoogleDataSource.GADataRetriever().RetrieveData(metric);
            Assert.IsNotNull(dp);
            Assert.IsNotNull(dp.Data);
            Debug.WriteLine(JsonConvert.SerializeObject(dp.Data,Formatting.Indented));
            Assert.IsNotNull(dp.Data.Rows);
            Assert.AreNotEqual(0, dp.Data.Rows.Count);
            Assert.AreEqual(dp.Data.Dimensions.Count,
            dp.Data.Rows[0].Dimensions.Count, "dimension count");
            Assert.AreEqual(dp.Data.Metrics.Count, dp.Data.Rows[0].Metrics.Count, "metrics count");

            dp.MetricId = metric.Id;
            await ((IMetricDataPointSaver)new EF.MetricDatapointSaverForEF()).SaveAsync(metric, dp, dp.Data);
        }
        private static AnalyticsService GetAnalyticsService()
        {

            var init = new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = GoogleApiHelper.GetServiceAccountCredential(),
                ApplicationName = "csgp.CentralizedAnalytics",
                ApiKey = GoogleApiHelper.ApiKey //" AIzaSyCdsV7TUb9UqZsgX06QSSHSzl5hNhDM5oE "// " AIzaSyA6rZhswdOwBREU7a2N4MFe92 -ZIrYW62c",

            };
            return new AnalyticsService(init);
        }
    }
}
