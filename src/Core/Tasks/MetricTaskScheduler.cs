﻿using csgp.CentralizedAnalytics.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public class MetricTaskScheduler
    {
        IRecurringTaskScheduler _scheduler;
        public MetricTaskScheduler(IRecurringTaskScheduler scheduler)
        {
            _scheduler = scheduler;
        }
        public string GetTaskId(Metric metric)
        {
            return string.Format("metric {0} ({1}/{2})", metric.Id, metric.TenantId, metric.DataSourceId);
        }
        public void ScheduleMetric(Metric metric)
        {
            if (metric == null)
            {
                throw new ArgumentNullException("metric");
            }

            string id = GetTaskId(metric);

            _scheduler.AddOrUpdate<MetricTaskRunner>(id, runner => runner.EnqueueMetric(metric.Id),metric.Schedule);
        }
        public void RemoveScheduledMetric(Metric metric)
        {
            if (metric == null)
            {
                throw new ArgumentNullException("metric");
            }

            string id = GetTaskId(metric);
            _scheduler.RemoveIfExists(id);
        }
    }
}
