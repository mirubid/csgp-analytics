namespace csgp.CentralizedAnalytics.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<csgp.CentralizedAnalytics.EF.AnalyticsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(csgp.CentralizedAnalytics.EF.AnalyticsDbContext context)
        {
            EF.Seeder.Seed(context);
            context.SaveChanges();
        }
    }
}
