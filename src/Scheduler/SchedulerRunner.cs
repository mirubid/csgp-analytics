﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Hangfire;
using Hangfire.SqlServer;
using Autofac;
using Autofac.Configuration;

namespace csgp.CentralizedAnalytics
{
    public class SchedulerRunner:IDisposable
    {
        BackgroundJobServer _jobServer;
        IContainer _ioc;

        public void Start()
        {
            if (_jobServer != null) { return; }

            var options = new SqlServerStorageOptions()
            {
                QueuePollInterval = TimeSpan.FromSeconds(5)
            };
            GlobalConfiguration.Configuration.UseSqlServerStorage("hangfire", options);

            var json_settings = new Newtonsoft.Json.JsonSerializerSettings()
            {
                DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind
            };

            Newtonsoft.Json.JsonConvert.DefaultSettings = () => json_settings;

            Hangfire.Common.JobHelper.SetSerializerSettings(json_settings);

            var builder = new Autofac.ContainerBuilder();
            

            builder.RegisterModule(new ConfigurationSettingsReader("autofac","autofac.config"));

            _ioc = builder.Build();
            GlobalConfiguration.Configuration.UseAutofacActivator(_ioc);
            _jobServer = new BackgroundJobServer();
            
            
        }
        public void Stop()
        {
            if (_jobServer != null)
            {
                _jobServer.Dispose();
                _jobServer = null;
            }
            if (_ioc != null) { 
                _ioc.Dispose();
                _ioc = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Stop();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SchedulerRunner() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
