﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
	public class DataSourceTypeDTO
    {
		public System.String Id { get; set; }
		public csgp.CentralizedAnalytics.DataSourceType.DataSourceDirection Direction { get; set; }
		public System.String Description { get; set; }

        public static System.Linq.Expressions.Expression<Func< DataSourceType,  DataSourceTypeDTO>> SELECT =
            x => new  DataSourceTypeDTO
            {
                Id = x.Id,
                Direction = x.Direction,
                Description = x.Description,
            };

	}
}