﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    /// <summary>
    /// generalized info about a query related to a metric data point
    /// Used to retrieve some general info from DataRetriever
    /// </summary>
    public class MetricQuery
    {
        public DateTime QueryStartDate { get; set; }
        public DateTime QueryEndDate { get; set; }
    }
}
