﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public interface ICmdConfigurator
    {
        /// <summary>
        /// configure commandline options
        /// </summary>
        /// <param name="prototype">e.g. f|flag|f<br/>v|val=</param>
        /// <param name="description">a description of what the option does</param>
        /// <param name="action">use option to set something within the cmdlet</param>
        /// <returns></returns>
        ICmdConfigurator AddOption(string prototype, string description, Action<string> action);

        ICmdConfigurator SetDescription(string description);

    }
}
