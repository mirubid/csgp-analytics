﻿	CREATE VIEW dbo.{1}
	AS
	SELECT d.Id [Id]
		  ,dp.Id [MetricDataPointId]
		  ,[MetricId]
		  ,[CreateDateUTC]
		  ,[QueryStartDate]
		  ,[QueryEndDate]
		  ,{2}
	  FROM [dbo].[MetricDataPoint] dp
	  JOIN [dbo].[{0}] d
		on dp.Id = d.MetricDataPointId
	 WHERE MetricId = {3}
