﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autofac;
using Autofac.Configuration;
using NLog;

namespace csgp.CentralizedAnalytics
{
    internal class ConsoleContext
    {
        private readonly IEnumerable<string> _args;
        private IContainer _ioc;
        ILogger _logger = LogManager.GetCurrentClassLogger();
        private ICmd _cmd = null;

        internal void InitIocContainer()
        {
            

            var builder = new Autofac.ContainerBuilder();
            

            builder.RegisterModule(new ConfigurationSettingsReader("autofac","autofac.config"));
            
            _ioc = builder.Build();

            _ioc.Resolve<IEnumerable<IStartup>>()
                .ToList()
                .ForEach(e=>e.Configure());
        }
        private ICmd ResolveCmd(ILifetimeScope ioc,string cmdName)
        {
            
            _logger.Debug("command name: {0}", cmdName);
            try
            {
                return ioc.ResolveNamed<ICmd>(cmdName);
            }catch(Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }
        public int Run(IEnumerable<string> arguments)
        {
            string[] args = (arguments == null) ? new string[0] : arguments.ToArray();

                
            _logger.Debug("Run");

            bool help = false;
            bool debug = false;
            string cmd = string.Empty;

                
            var options = new OptionParser();
            options
                .AddOption("?|help", "print help", f => { help = (f != null); })
                .AddOption("debugger", "attach debugger", f => { debug = (f != null); })
                ;
            if (args.Length > 0)
            {
                cmd = args[0];
                
            }
            if (_ioc == null)
            {
                InitIocContainer();
            }
            using (var ioc = _ioc.BeginLifetimeScope())
            {
                _cmd = ResolveCmd(ioc,cmd);        
            



                if (_cmd != null)
                {
                    _cmd.ConfigureOptions(options);
                }
                // parse now that we know which command to use
                var extra = options.Parse(args.Skip(1));


                if (debug)
                {
                    System.Diagnostics.Debugger.Launch();
                }
                if (help)
                {
                    WriteHelp(_cmd, options);
                    return 0;
                }
                if (_cmd != null)
                {
                    try
                    {
                        return _cmd.Execute(extra);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Command Failed: {0}", cmd);
                        _logger.Error(ex);

                        Console.Error.WriteLine("Command Failed: {0}", cmd);
                        Console.Error.WriteLine(ex.Message);

                        return -1;
                    }
                }
            }
            return 0;
        }
        internal void WriteHelp(ICmd cmd, OptionParser options)
        {
            if (cmd != null) { 
                Console.WriteLine("Usage for {0}", cmd.GetType().Name);
                Console.WriteLine("command names: {0}", String.Join(",", cmd.Names));
            }else
            {
                Console.WriteLine("available commands:");
                _ioc.Resolve<IEnumerable<ICmd>>()
                    .Select(e => String.Join(",", e.Names))
                    .ToList()
                    .ForEach(Console.WriteLine);

            }
            options.WriteHelp(Console.Out);
        }
    }
}
