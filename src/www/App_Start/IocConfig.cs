﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Autofac.Configuration;
using System.Reflection;

namespace csgp.CentralizedAnalyticsProcessing
{
    public class IocConfig
    {

        internal static void Configure(HttpConfiguration configuration)
        {

            var builder = new Autofac.ContainerBuilder();


            builder.RegisterModule(new ConfigurationSettingsReader("autofac","autofac.config"));

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(configuration);

            var ioc = builder.Build();

            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(ioc);

        }
    }
}