﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.Tasks
{
    public class UnscheduleMetricCmd: ICmd
    {

        private string _metric_id;

        private csgp.CentralizedAnalytics.EF.AnalyticsDbContext db;
        private MetricTaskScheduler _scheduler;

        public UnscheduleMetricCmd(EF.AnalyticsDbContext dbContext, MetricTaskScheduler scheduler)
        {
            this.db = dbContext;
            _scheduler = scheduler;
        }
        public string[] Names
        {
            get
            {
                return new[] { "unschedule-metric" };
            }
        }

        public void ConfigureOptions(ICmdConfigurator options)
        {
            options.SetDescription("unschedule a specific Metric");

            options.AddOption("m|metric=", "metric id", (e) => _metric_id = e);
                
        }

        public int Execute(List<string> extraArgs)
        {

            int metric_id;
            if (int.TryParse(_metric_id,out metric_id))
            {

                var metric = db.Metrics.FirstOrDefault(e => e.Id == metric_id);
                if (metric == null)
                {
                    throw new ArgumentException(string.Format("Metric not found: {0}", metric_id));
                }


                _scheduler.RemoveScheduledMetric(metric);
                metric.Schedule = null;
                db.SaveChanges();
                return 0;
            }
            else
            {
                throw new ArgumentException("metric|m (int)  must be specified");
            }
            
            
        }
    }
}
