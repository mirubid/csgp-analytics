﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
	public class MetricDTO
    {
		public string DataDestination_Id { get; set; }
		public string DataSource_Id { get; set; }
		public System.Int32 Id { get; set; }
		public System.String Name { get; set; }
		public System.String Description { get; set; }
		public System.String TenantId { get; set; }
		public System.String DataSourceId { get; set; }
		public System.String DataDestinationId { get; set; }
		public System.String Configuration { get; set; }
		public System.String Schedule { get; set; }
		public System.Boolean EnableAggregation { get; set; }
		public System.DateTime CreateDateUTC { get; set; }
		public System.DateTime UpdateDateUTC { get; set; }

        public static System.Linq.Expressions.Expression<Func< Metric,  MetricDTO>> SELECT =
            x => new  MetricDTO
            {
                DataDestination_Id = x.DataDestination.Id,
                DataSource_Id = x.DataSource.Id,
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                TenantId = x.TenantId,
                DataSourceId = x.DataSourceId,
                DataDestinationId = x.DataDestinationId,
                Configuration = x.Configuration,
                Schedule = x.Schedule,
                EnableAggregation = x.EnableAggregation,
                CreateDateUTC = x.CreateDateUTC,
                UpdateDateUTC = x.UpdateDateUTC,
            };

	}
}