﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        protected override void OnBeforeInstall(IDictionary savedState)
        {
            // add some arguments to the service command
            var assp = Context.Parameters["assemblypath"];
            string parameter = "run-service";

            // add the start up parameters
            Context.Parameters["assemblypath"] = string.Format("\"{0}\" {1}", assp, parameter);

            base.OnBeforeInstall(savedState);
        }
    }
}
