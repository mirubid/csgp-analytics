﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using csgp.CentralizedAnalytics.Tasks;
using csgp.CentralizedAnalytics;

namespace csgp.CentralizedAnalytics
{
    public class CentralizedAnalyticsConfiguration:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterAssemblyTypes(typeof(Metric).Assembly);


            RegisterMetricRetrievers(builder);
            RegisterDatapointSavers(builder);

            builder.RegisterType<Tasks.MetricTaskRunner>()
                .InstancePerDependency();

            builder.RegisterType<RecurringTaskScheduler>().AsImplementedInterfaces();
            builder.RegisterType<MetricTaskScheduler>();

            builder.RegisterType<QueueMetricCmd>().Named<ICmd>("queue-metric");
            builder.RegisterType<EF.UpgradeCmd>().Named<ICmd>("upgrade-db");
            builder.RegisterType<ScheduleMetricCmd>().Named<ICmd>("schedule-metric");
            builder.RegisterType<UnscheduleMetricCmd>().Named<ICmd>("unschedule-metric");
        }
        private void RegisterMetricRetrievers(ContainerBuilder builder)
        {
            // datasource specific retrievers are named by their datasource type id
            builder.RegisterType<GoogleDataSource.GADataRetriever>()
                .Named<IMetricRetriever>("GA");


            builder.Register<Func<string, IMetricRetriever>>(
                e =>
                {
                    var c = e.Resolve<IComponentContext>();
                    return (name) => c.ResolveNamed<IMetricRetriever>(name);
                }
                );
        }
        private void RegisterDatapointSavers(ContainerBuilder builder)
        {
            builder.RegisterType<NoOpMetricDataPointSaver>().AsImplementedInterfaces()
                .Named<IMetricDataPointSaver>("NULL");

            builder.RegisterType<EF.MetricDatapointSaverForEF>()
                .Named<IMetricDataPointSaver>("LocalDb");

            builder.Register<Func<string, IMetricDataPointSaver>>(
                e =>
                {
                    var c = e.Resolve<IComponentContext>();
                    return (name) => c.ResolveNamed<IMetricDataPointSaver>(name);
                }
                );
        }
    }
}
