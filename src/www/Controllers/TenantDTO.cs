﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
	public class TenantDTO
    {
		public int DataSources_Count { get; set; }
		public int Metrics_Count { get; set; }
		public System.String Id { get; set; }
		public System.String Description { get; set; }
		public System.DateTime CreateDateUTC { get; set; }
		public System.DateTime UpdateDateUTC { get; set; }

        public static System.Linq.Expressions.Expression<Func< Tenant,  TenantDTO>> SELECT =
            x => new  TenantDTO
            {
                DataSources_Count = x.DataSources.Count(),
                Metrics_Count = x.Metrics.Count(),
                Id = x.Id,
                Description = x.Description,
                CreateDateUTC = x.CreateDateUTC,
                UpdateDateUTC = x.UpdateDateUTC,
            };

	}
}