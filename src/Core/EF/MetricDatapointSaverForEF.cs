﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.EF
{
    public class MetricDatapointSaverForEF : IMetricDataPointSaver
    {
        
        async Task IMetricDataPointSaver.SaveAsync(Metric metric, MetricDataPoint datapoint, MetricData data)
        {
            if (metric == null)
            {
                throw new ArgumentNullException("metric");
            }

            if (datapoint == null)
            {
                throw new ArgumentNullException("datapoint");
            }
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            await SaveMetricDataPointAsync(datapoint);
            
            await new MetricDataSaverDb().SaveAsync(metric, datapoint, data);
        }
        async Task SaveMetricDataPointAsync(MetricDataPoint datapoint)
        {
            using (var db = new AnalyticsDbContext())
            {
                MetricDataPoint existing = null;
                if (datapoint.Id != 0)
                {
                    // subsequent page, do an update of the PageIndex rather than insert
                    existing = await db.MetricDataPoints.Where(e => e.Id == datapoint.Id).FirstOrDefaultAsync().ConfigureAwait(false);

                }

                if (existing == null)
                {
                    db.MetricDataPoints.Add(datapoint);
                }else
                {
                    existing.PageIndex = datapoint.PageIndex;
                }

                await db.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}
