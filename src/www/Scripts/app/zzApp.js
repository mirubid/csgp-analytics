﻿angular.module('app', modules);
angular.module('app').controller('MainMenu', function ($scope) {
    $scope.modules = modules;
});
angular.module('app').config(function ($httpProvider, $provide) {
    $provide.factory('prefixHttpRequest', function () {
        return {
            request: function (config) {
                
                if (config.url && config.url[0] === '/') {
                    config.url = '/analytics' + config.url;
                }

                return config
            }
        }
    });
    $httpProvider.interceptors.push('prefixHttpRequest');
    
});