﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    public class MetricData
    {
        public List<string> Dimensions { get; set; }
        public List<string> Metrics { get; set; }
        public List<MetricDataRow> Rows { get; set; }
    }
    public class MetricDataRow
    {
        public List<string> Dimensions { get; set; }
        public List<double> Metrics { get; set; }
    }
}
