﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.EF
{
    public class UpgradeCmd : ICmd
    {
        public string[] Names
        {
            get
            {
                return new[] { "upgrade-db","migrate-db" };
            }
        }

        public void ConfigureOptions(ICmdConfigurator options)
        {
            options.SetDescription("execute EF migration to update the db");
        }

        public int Execute(List<string> extraArgs)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AnalyticsDbContext, Migrations.Configuration>());

            using (var db = new EF.AnalyticsDbContext())
            {
                var metrics = db.Metrics.Count();

            }

            return 0;
        }
    }
}
