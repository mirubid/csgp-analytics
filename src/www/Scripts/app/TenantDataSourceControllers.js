﻿ var  modules = modules || [];
(function () {
    'use strict';
    modules.push('TenantDataSource');

    angular.module('TenantDataSource',['ngRoute'])
    .controller('TenantDataSource_list', ['$scope', '$http','$location', function($scope, $http, $location){

        console.log('TenantDataSource scope', $scope);
        $http.get('/Api/TenantDataSource/',{params:$location.search()})
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('TenantDataSource_details', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

        

        $http.get('/Api/TenantDataSource/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('TenantDataSource_create', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        console.log('TenantDataSource_create', $scope);

        $scope.data = {__mode:'create'};
                $http.get('/Api/DataSourceType/')
        .then(function(response){$scope.DataSourceTypeId_options = response.data;});
                $http.get('/Api/Tenant/')
        .then(function(response){$scope.TenantId_options = response.data;});
        
        $scope.save = function(){
            $http.post('/Api/TenantDataSource/', $scope.data)
            .then(function(response){ $location.path("TenantDataSource"); });
        }

    }])
    .controller('TenantDataSource_edit', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){
		$scope.data.__mode='edit';
        $http.get('/Api/TenantDataSource/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

                $http.get('/Api/DataSourceType/')
        .then(function(response){$scope.DataSourceTypeId_options = response.data;});
                $http.get('/Api/Tenant/')
        .then(function(response){$scope.TenantId_options = response.data;});
        
        $scope.save = function(){
            $http.put('/Api/TenantDataSource/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("TenantDataSource"); });
        }

    }])
    .controller('TenantDataSource_delete', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/TenantDataSource/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});
        $scope.save = function(){
            $http.delete('/Api/TenantDataSource/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("TenantDataSource"); });
        }

    }])

    .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
            .when('/TenantDataSource', {
                title: 'TenantDataSource - List',
                templateUrl: '/Static/TenantDataSource_List',
                controller: 'TenantDataSource_list'
            })
            .when('/TenantDataSource/Create', {
                title: 'TenantDataSource - Create',
                templateUrl: '/Static/TenantDataSource_Edit',
                controller: 'TenantDataSource_create'
            })
            .when('/TenantDataSource/Edit/:id', {
                title: 'TenantDataSource - Edit',
                templateUrl: '/Static/TenantDataSource_Edit',
                controller: 'TenantDataSource_edit'
            })
            .when('/TenantDataSource/Delete/:id', {
                title: 'TenantDataSource - Delete',
                templateUrl: '/Static/TenantDataSource_Delete',
                controller: 'TenantDataSource_delete'
            })
            .when('/TenantDataSource/:id', {
                title: 'TenantDataSource - Details',
                templateUrl: '/Static/TenantDataSource_Details',
                controller: 'TenantDataSource_details'
            })
    }])
;

})();
