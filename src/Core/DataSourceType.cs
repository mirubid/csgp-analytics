﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace csgp.CentralizedAnalytics
{
    [ReadOnly(true),DisplayColumn("Id")]
    public class DataSourceType
    {
        /// <summary>
        /// whether this datasource type can be used as a source or destination
        /// </summary>
        [Flags][ReadOnly(true)]
        public enum DataSourceDirection:byte
        {
            Source=0x1,
            Destination=0x2,
            Both=Source|Destination
        }
        [StringLength(50)]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Required,ReadOnly(true)]
        public DataSourceDirection Direction { get; set; } 

        [Required]
        public string Description { get; set; }
    }
}
