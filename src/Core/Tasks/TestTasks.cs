﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.Tasks
{
    /// <summary>
    /// some tasks purely for testing hangfire
    /// </summary>
    public class TestTasks
    {
        public  void SucceedImmediately()
        {
            // no op
        }
        public void SucceedWithDelay(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }

        public void FailImmediately()
        {
            throw new Exception("this task failed!");
        }
    }
}
