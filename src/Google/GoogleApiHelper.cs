﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using System.Configuration;

namespace csgp.CentralizedAnalytics
{
    public static class GoogleApiHelper
    {
        private static ServiceAccountCredential _serviceCredential;
        private static string _apiKey;

        public static  ServiceAccountCredential ServiceCredential{
            get
            {
                if (_serviceCredential == null)
                {
                    _serviceCredential = GetServiceAccountCredential();
                }
                return _serviceCredential;
            }
            set
            {
                _serviceCredential = value;
            }
        }
        public static string ApiKey
        {
            get
            {
                if (string.IsNullOrEmpty(_apiKey))
                {
                    _apiKey = Utility.AppSetting("google-api-key");
                }
                return _apiKey;
            }
        }
        /// <summary>
        /// use client_secrets.json to create a ServiceAccountCredential
        /// </summary>
        /// <param name="id">account id. looks like an email address</param>
        /// <param name="private_key">private key</param>
        /// <returns></returns>
        public static ServiceAccountCredential GetServiceAccountCredential(string id,string private_key)
        {
            if (private_key == null)
            {
                throw new ArgumentNullException("private_key");
            }
            if (private_key.StartsWith("{"))
            {
                var cred = Newtonsoft.Json.Linq.JObject.Parse(private_key);
                private_key = (string)cred["private_key"];
            }
            // problem: Google.GoogleApiException: Google.Apis.Requests.RequestError
            // User does not have any Google Analytics account. [403]
            // solution: http://stackoverflow.com/questions/12837748/analytics-google-api-error-403-user-does-not-have-any-google-analytics-account
            //var cred = Newtonsoft.Json.Linq.JObject.Parse(client_secrets_json);
            var initializer = new ServiceAccountCredential.Initializer(id)
            {
                Scopes=Enumerable.Repeat(Google.Apis.Analytics.v3.AnalyticsService.Scope.AnalyticsReadonly,1)
            };

            initializer = initializer.FromPrivateKey(private_key);

            ////https://accounts.google.com/o/oauth2/auth?redirect_uri=https%3A%2F%2Fdevelopers.google.com%2Foauthplayground&response_type=code&client_id=407408718192.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&approval_prompt=force&access_type=offline
            //var init = new Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer()
            //{
            //    ClientSecrets=new ClientSecrets()
            //    {
            //        ClientId="",
            //        ClientSecret=""
            //    },
            //    Scopes = new[] {"https://www.googleapis.com/auth/analytics.readonly"},
                
                
            //};
            //new GoogleAuthorizationCodeFlow(init).CreateAuthorizationCodeRequest("").

            return new ServiceAccountCredential(initializer);
        }
        public static ServiceAccountCredential GetServiceAccountCredential()
        {
            return GetServiceAccountCredential(
            Utility.AppSetting("google-service-account-userid"),
            Utility.AppSetting("google-service-account-privatekey")
            );


        }
    }
}
