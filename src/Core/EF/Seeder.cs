﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csgp.CentralizedAnalytics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.Entity.Validation;

namespace csgp.CentralizedAnalytics.EF
{
    public class Seeder
    {
        public static void Seed(AnalyticsDbContext context)
        {
            using (var txn = context.Database.BeginTransaction()) {
                #region data source types
                /*
INSERT INTO dbo.DataSourceType (Id,	Direction,	Description)

VALUES 
('Db',	3	,'Database (any ADO.NET data source)'),
('GA',	1	,'Google Analytics'),
('LocalDb',	3	,'Centralized Analytic''s internal database'),
('NULL',	2,	'a data source used as a sink for downloaded metrics (useful for testing).')

                 */
                if (!context.TenantDataSources.Any())
                {
                    if (!context.TenantDataSources.Any(e => e.Id == "GA"))
                        context.DataSourceTypes.Add(new DataSourceType() { Id = "GA", Description = "Google Analytics", Direction = DataSourceType.DataSourceDirection.Source });
                    if (!context.TenantDataSources.Any(e => e.Id == "Db"))
                        context.DataSourceTypes.Add(new DataSourceType() { Id = "Db", Description = "Database (any ADO.NET data source)", Direction = DataSourceType.DataSourceDirection.Both });
                    if (!context.TenantDataSources.Any(e => e.Id == "LocalDb"))
                        context.DataSourceTypes.Add(new DataSourceType() { Id = "LocalDb", Description = "Centralized Analytic's internal database", Direction = DataSourceType.DataSourceDirection.Both });
                    if (!context.TenantDataSources.Any(e => e.Id == "NULL"))
                        context.DataSourceTypes.Add(new DataSourceType() { Id = "NULL", Description = "a data source used as a sink for downloaded metrics (useful for testing).", Direction = DataSourceType.DataSourceDirection.Destination });
                    context.SaveChanges();
                }
                #endregion
                bool addTenant = false;
                if (context.Tenants.Any(e => e.Id == "Test"))
                {
                    return;
                }

                addTenant = true;

                context.Tenants.Add(
                    new Tenant()
                    {
                        Id = "Test",
                        Description = "tenant for testing",
                        CreateDateUTC = DateTime.UtcNow
                    }
                );

                context.SaveChanges();
                
                var ds_config = JObject.Parse(Core.Properties.Resources.ga_test_datasource_config);



                #region tenant data sources
                if (addTenant)
                {
                    context.TenantDataSources.Add(new TenantDataSource()
                    {
                        Id = "NULL Test",
                        TenantId = "Test",

                        DataSourceTypeId = "NULL"

                    });
                    context.TenantDataSources.Add(new TenantDataSource()
                    {
                        Id = "LocalDb Test",
                        TenantId = "Test",
                        DataSourceTypeId = "LocalDb"

                    });


                    context.TenantDataSources.Add(new TenantDataSource()
                    {
                        Id = "GA Test",
                        //Name = "Test account for Google Api",
                        TenantId = "Test",
                        Configuration = ds_config.ToString(Formatting.None),
                        DataSourceTypeId = "GA",

                    });
                }
                #endregion

                context.SaveChanges();

                if (addTenant)
                {
                    context.Metrics.Add(new Metric()
                    {
                        TenantId = "Test",
                        Name = "GA Test Metric",
                        Description = "GA data query for testing",
                        Configuration = Core.Properties.Resources.ga_test_metric,
                        DataSourceId = "GA Test",
                        DataDestinationId = "LocalDb Test",
                        Schedule = "* * * * *"      // every minute   http://en.wikipedia.org/wiki/Cron#CRON_expression       
                    });
                    context.SaveChanges();
                }
                txn.Commit();
            }
        }
    }
}
