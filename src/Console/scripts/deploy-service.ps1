﻿
$drop = '.\debug'
$exename='process-jobs.exe'
$run = '.\run'

function get-status(){
    & "$run\$exename" service status
}
function stop-service(){
    & "$run\$exename" service stop
}
function start-service(){
    & "$run\$exename" service start
}
if(test-path "$run\$exename"){
    $status = get-status
    write-host 'stopping service' -ForegroundColor Green
	if($status -eq 'Running'){
        stop-service
    }
    $tries=0
    while('Running' -eq $status){
        $tries++
        if($tries -gt 10){
            write-host "service has not stopped," -ForegroundColor Green
            exit
        }
        write-host "waiting for service to stop... ($tries)" -ForegroundColor Green
        start-sleep -s 2
        $status = get-status
    }
}

robocopy $drop $run /XF *.config *.log
start-service