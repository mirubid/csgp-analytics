﻿ var  modules = modules || [];
(function () {
    'use strict';
    modules.push('Tenant');

    angular.module('Tenant',['ngRoute'])
    .controller('Tenant_list', ['$scope', '$http','$location', function($scope, $http, $location){

        $http.get('/Api/Tenant/',{params:$location.search()})
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('Tenant_details', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

        $http.get('/Api/Tenant/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('Tenant_create', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $scope.data = {__mode:'create'};
        
        $scope.save = function(){
            $http.post('/Api/Tenant/', $scope.data)
            .then(function(response){ $location.path("Tenant"); });
        }

    }])
    .controller('Tenant_edit', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){
		$scope.data.__mode='edit';
        $http.get('/Api/Tenant/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

        
        $scope.save = function(){
            $http.put('/Api/Tenant/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("Tenant"); });
        }

    }])
    .controller('Tenant_delete', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/Tenant/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});
        $scope.save = function () {
            $http.delete('/Api/Tenant/' + $routeParams.id, $scope.data)
            .then(function (response) { $location.path("Tenant"); });
        };


    }])
    
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                title: 'Tenant - List',
                templateUrl: '/Static/Tenant_List',
                controller: 'Tenant_list'
            })
            .when('/Tenant', {
                title: 'Tenant - List',
                templateUrl: '/Static/Tenant_List',
                controller: 'Tenant_list'
            })
            .when('/Tenant/Create', {
                title: 'Tenant - Create',
                templateUrl: '/Static/Tenant_Edit',
                controller: 'Tenant_create'
            })
            .when('/Tenant/Edit/:id', {
                title: 'Tenant - Edit',
                templateUrl: '/Static/Tenant_Edit',
                controller: 'Tenant_edit'
            })
            .when('/Tenant/Delete/:id', {
                title: 'Tenant - Delete',
                templateUrl: '/Static/Tenant_Delete',
                controller: 'Tenant_delete'
            })
                .when('/Tenant/DataSources', {
                    title: 'Tenant/DataSources',
                    templateUrl: '/Static/TenantDataSource_List',
                    controller: 'TenantDataSource_List'
                })
            .when('/Tenant/:id', {
                title: 'Tenant - Details',
                templateUrl: '/Static/Tenant_Details',
                controller: 'Tenant_details'
            })
    }])
;

})();
