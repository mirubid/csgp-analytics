﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
	public class TenantDataSourceDTO
    {
		public string DataSourceType_Id { get; set; }
		public int Metrics_Count { get; set; }
		public string Tenant_Id { get; set; }
		public System.String TenantId { get; set; }
		public System.String Id { get; set; }
		public System.String Configuration { get; set; }
		public System.String DataSourceTypeId { get; set; }
		public System.DateTime CreateDateUTC { get; set; }
		public System.DateTime UpdateDateUTC { get; set; }

        public static System.Linq.Expressions.Expression<Func< TenantDataSource,  TenantDataSourceDTO>> SELECT =
            x => new  TenantDataSourceDTO
            {
                DataSourceType_Id = x.DataSourceType.Id,
                Metrics_Count = x.Metrics.Count(),
                Tenant_Id = x.Tenant.Id,
                TenantId = x.TenantId,
                Id = x.Id,
                Configuration = x.Configuration,
                DataSourceTypeId = x.DataSourceTypeId,
                CreateDateUTC = x.CreateDateUTC,
                UpdateDateUTC = x.UpdateDateUTC,
            };

	}
}