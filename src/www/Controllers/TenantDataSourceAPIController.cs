﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace csgp.CentralizedAnalytics
{
    public class TenantDataSourceController : ApiController
    {
        private csgp.CentralizedAnalytics.EF.AnalyticsDbContext db = new csgp.CentralizedAnalytics.EF.AnalyticsDbContext();

        public IQueryable<TenantDataSourceDTO> GetTenantDataSources(int pageSize = 10
                        ,System.String TenantId = null
                        ,System.String DataSourceTypeId = null
                        ,int? Direction = null
                )
        {
            var model = db.TenantDataSources.AsQueryable();
                                if(TenantId != null){
                        model = model.Where(m=> m.TenantId == TenantId);
                    }
                                if(DataSourceTypeId != null){
                        model = model.Where(m=> m.DataSourceTypeId == DataSourceTypeId);
                    }
            if(Direction != null)
            {
                DataSourceType.DataSourceDirection dir = (DataSourceType.DataSourceDirection)Direction;

                model = model.Where(m => (m.DataSourceType.Direction & dir) != 0);
            }
                        
            return model.Select(TenantDataSourceDTO.SELECT).Take(pageSize);
        }


        [ResponseType(typeof(TenantDataSourceDTO))]
        public async Task<IHttpActionResult> GetTenantDataSource(string id)
        {
            var model = await db.TenantDataSources.Select(TenantDataSourceDTO.SELECT).FirstOrDefaultAsync(x => x.TenantId == id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        public async Task<IHttpActionResult> PutTenantDataSource(string id, TenantDataSource model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.TenantId)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TenantDataSourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(TenantDataSourceDTO))]
        public async Task<IHttpActionResult> PostTenantDataSource(TenantDataSource model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TenantDataSources.Add(model);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TenantDataSourceExists(model.TenantId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            var ret = await db.TenantDataSources.Select(TenantDataSourceDTO.SELECT).FirstOrDefaultAsync(x => x.TenantId == model.TenantId);
            return CreatedAtRoute("DefaultApi", new { id = model.TenantId }, model);
        }

        [ResponseType(typeof(TenantDataSourceDTO))]
        public async Task<IHttpActionResult> DeleteTenantDataSource(string id)
        {
            TenantDataSource model = await db.TenantDataSources.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.TenantDataSources.Remove(model);
            await db.SaveChangesAsync();
            var ret = await db.TenantDataSources.Select(TenantDataSourceDTO.SELECT).FirstOrDefaultAsync(x => x.TenantId == model.TenantId);
            return Ok(ret);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TenantDataSourceExists(string id)
        {
            return db.TenantDataSources.Count(e => e.TenantId == id) > 0;
        }
    }
}