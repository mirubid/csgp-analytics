﻿using csgp.CentralizedAnalytics.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace csgp.CentralizedAnalytics
{
    public class RecurringTaskScheduler : IRecurringTaskScheduler
    {
        void IRecurringTaskScheduler.AddOrUpdate<T>(string taskId, Expression<Action<T>> expression, string schedule)
        {
            Hangfire.RecurringJob.AddOrUpdate<T>(taskId, expression, schedule);
        }
        void IRecurringTaskScheduler.RemoveIfExists(string taskId)
        {
            Hangfire.RecurringJob.RemoveIfExists(taskId);
        }
    }
}
