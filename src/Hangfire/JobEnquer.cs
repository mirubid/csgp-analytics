﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using csgp.CentralizedAnalytics;

namespace csgp.CentralizedAnalytics
{
    public class HangfireJobEnquer : IJobEnquer
    {
        void IJobEnquer.Enqueue<T>(Expression<Action<T>> expression)
        {
            BackgroundJob.Enqueue<T>(expression);
        }
    }
}
