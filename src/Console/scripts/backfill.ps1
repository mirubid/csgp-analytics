﻿$metricId = 6
$startDate = [DateTime]::Parse('July 16, 2016')
$now = [DateTime]::Now
$days = ($now - $startDate).Days
1..$days | %{.\debug\analytics.exe queue-metric -m="$metricId" -da="$_"}
