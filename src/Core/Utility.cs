﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace csgp.CentralizedAnalytics
{
    public static class Utility
    {
        private static ILogger _logger = LogManager.GetCurrentClassLogger();
        internal static ILogger Logger { get { return _logger; } }

        public static string AppSetting(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
        public static string ConnectionString(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            return System.Configuration.ConfigurationManager.ConnectionStrings[key]?.ConnectionString;
        }
    }
}
