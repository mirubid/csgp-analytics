﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hangfire;
using System.Diagnostics;

namespace csgp.CentralizedAnalytics
{
    [TestClass]
    public class SchedulerTests
    {
        [TestMethod]
        public void ShouldBeAbleToScheduleATask()
        {
            BackgroundJob.Enqueue(() => Console.WriteLine("unit test working?"));

            BackgroundJob.Enqueue<Tasks.TestTasks>(e=>e.FailImmediately());

            BackgroundJob.Enqueue<Tasks.TestTasks>(e => e.SucceedWithDelay(5000));


        }
        [TestMethod]
        public void ShouldBeAbleToScheduleATaskWithSomeDelay()
        {
            BackgroundJob.Schedule(() => Console.WriteLine("delayed task"),TimeSpan.FromHours(1));
        }

        [TestMethod]
        public void ShouldBeAbleToParseNDaysAgo()
        {
            DateTime now = DateTime.Parse("2016-06-06");
            DateTime yesterday = DateTimeUtility.ParseAsDate("yesterday");
            Debug.WriteLine(yesterday);

            DateTime today = DateTimeUtility.ParseAsDate("today");

            DateTime daysAgo0 = DateTimeUtility.ParseAsDate("0daysago");
            DateTime daysAgo1 = DateTimeUtility.ParseAsDate("1daysago");
            DateTime daysAgo2 = DateTimeUtility.ParseAsDate("2daysago");

            DateTime daysAgo1000 = DateTimeUtility.ParseAsDate("1000daysago");

            DateTime jan1_2016 = DateTimeUtility.ParseAsDate("2016-01-01");

            Assert.AreEqual(daysAgo0, today, @" daysAgo0 vs today");
            Assert.AreEqual(daysAgo1, yesterday, @" daysAgo1 vs yesterday");

            Debug.WriteLine("yesterday:{0}, today:{1} 1000daysago:{2}", yesterday, today, daysAgo1000);

            Assert.AreEqual(1000, (today-daysAgo1000).TotalDays, @" 1000 vs (today - daysAgo1000).TotalDays");

        }
    }
}
