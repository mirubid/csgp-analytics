﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    partial class AnalyticsSchedulerService : ServiceBase
    {
        private SchedulerRunner _task;

        public AnalyticsSchedulerService()
        {
            Program.Logger.Info("service constructor");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Program.Logger.Info("service OnStart");
            _task = new SchedulerRunner();
            _task.Start();
        }

        protected override void OnStop()
        {
            Program.Logger.Info("service OnStop");
            _task.Dispose();
            _task = null;
        }
    }
}
