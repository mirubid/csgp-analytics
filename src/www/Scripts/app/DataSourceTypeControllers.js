﻿ var  modules = modules || [];
(function () {
    'use strict';
    modules.push('DataSourceType');

    angular.module('DataSourceType',['ngRoute'])
    .controller('DataSourceType_list', ['$scope', '$http', function($scope, $http){

        $http.get('/Api/DataSourceType/')
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('DataSourceType_details', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

        $http.get('/Api/DataSourceType/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('DataSourceType_create', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $scope.data = {};
        
        $scope.save = function(){
            $http.post('/Api/DataSourceType/', $scope.data)
            .then(function(response){ $location.path("DataSourceType"); });
        }

    }])
    .controller('DataSourceType_edit', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/DataSourceType/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

        
        $scope.save = function(){
            $http.put('/Api/DataSourceType/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("DataSourceType"); });
        }

    }])
    .controller('DataSourceType_delete', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/DataSourceType/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});
        $scope.save = function(){
            $http.delete('/Api/DataSourceType/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("DataSourceType"); });
        }

    }])

    .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
            .when('/DataSourceType', {
                title: 'DataSourceType - List',
                templateUrl: '/Static/DataSourceType_List',
                controller: 'DataSourceType_list'
            })
            .when('/DataSourceType/Create', {
                title: 'DataSourceType - Create',
                templateUrl: '/Static/DataSourceType_Edit',
                controller: 'DataSourceType_create'
            })
            .when('/DataSourceType/Edit/:id', {
                title: 'DataSourceType - Edit',
                templateUrl: '/Static/DataSourceType_Edit',
                controller: 'DataSourceType_edit'
            })
            .when('/DataSourceType/Delete/:id', {
                title: 'DataSourceType - Delete',
                templateUrl: '/Static/DataSourceType_Delete',
                controller: 'DataSourceType_delete'
            })
            .when('/DataSourceType/:id', {
                title: 'DataSourceType - Details',
                templateUrl: '/Static/DataSourceType_Details',
                controller: 'DataSourceType_details'
            })
    }])
;

})();
