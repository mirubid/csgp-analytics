namespace csgp.CentralizedAnalytics.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PageIndex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MetricDataPoint", "PageIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MetricDataPoint", "PageIndex");
        }
    }
}
