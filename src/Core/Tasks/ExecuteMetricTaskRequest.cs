﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace csgp.CentralizedAnalytics.Tasks
{
    public class ExecuteMetricTaskRequest
    {
        public int MetricId { get; set; }

        /// <summary>
        /// id of previous MetricDatapoint if this is not the first page
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? MetricDatapointId { get; set; }

        public string UtcNow { get; set; }

        /// <summary>
        /// first page = 0
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int PageIndex { get; set; }
    }
}
