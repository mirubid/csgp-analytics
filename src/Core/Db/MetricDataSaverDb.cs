﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csgp.CentralizedAnalytics.Core;
using System.Data;
using System.Data.SqlClient;

namespace csgp.CentralizedAnalytics
{
    public class MetricDataSaverDb
    {
        public async Task SaveAsync(Metric metric,MetricDataPoint dp,MetricData data)
        {
            
            if (metric == null)
            {
                throw new ArgumentNullException("metric");
            }
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            await CreateTableAsync(metric, data);
            await CreateViewAsync(metric, data);
            await SaveData(metric, dp, data);

            

        }
        private async Task SaveData(Metric metric, MetricDataPoint dp, MetricData data)
        {
            var table = MakeDataTable(data);
            FillDataTable(table, dp, data);

            var adapter = MakeAdapter(table, data);
            using(var cnn = new SqlConnection(Utility.ConnectionString("CentralizedAnalyticsDb")))
            {
                adapter.InsertCommand.Connection = cnn;
                await cnn.OpenAsync();
                adapter.UpdateBatchSize = 500;
                adapter.Update(table);
            }
        }
        private SqlDataAdapter MakeAdapter(DataTable dt,MetricData data)
        {
            var adapter = new SqlDataAdapter();
            adapter.InsertCommand =
            new SqlCommand()
            {
                CommandType = CommandType.Text,
                CommandText = string.Format("INSERT INTO {0} ({1}) VALUES ({2})",
                        GetTableName(data),
                    String.Join(",",
                        Enumerable.Range(0, dt.Columns.Count)
                        .Select(e => dt.Columns[e].ColumnName)
                        ),
                    String.Join(",",
                        Enumerable.Range(0, dt.Columns.Count)
                        .Select(e => "@" + dt.Columns[e].ColumnName)
                        )
                        ),
                UpdatedRowSource = UpdateRowSource.None
            };
            adapter.InsertCommand.Parameters.Add(new SqlParameter("@MetricDataPointId",SqlDbType.Int,4,"MetricDataPointId"));
            for(var i = 0; i < data.Dimensions.Count; i++)
            {
                adapter.InsertCommand.Parameters.Add(new SqlParameter(string.Format("@dimension{0}", i), SqlDbType.VarChar,255, string.Format("dimension{0}", i)));
            }
            for (var i=0; i<data.Metrics.Count;i++)
            {                
                adapter.InsertCommand.Parameters.Add(new SqlParameter(string.Format("@metric{0}",i), SqlDbType.Float,8, string.Format("metric{0}", i)));
            }
            return adapter;
        }
        private DataTable MakeDataTable(MetricData data)
        {
            DataTable dt = new DataTable(GetTableName(data), "dbo");
            dt.Columns.Add("MetricDataPointId", typeof(int));
            for(var i=0;i<data.Dimensions.Count;i++)
            {
                dt.Columns.Add(string.Format("dimension{0}", i), typeof(string));
            }
            for (var i = 0; i < data.Metrics.Count; i++)
            {
                dt.Columns.Add(string.Format("metric{0}", i), typeof(double));
            }

            return dt;
        }
        private void FillDataTable(DataTable table,MetricDataPoint dp, MetricData data)
        {
            int d = data.Dimensions.Count;
            int m = data.Metrics.Count;

            foreach(var row in data.Rows)
            {
                var dr = table.NewRow();
                dr[0] = dp.Id;
                for(var i = 0; i < row.Dimensions.Count; i++)
                {
                    dr[1+i] = row.Dimensions[i];
                }
                for (var i = 0; i < row.Metrics.Count; i++)
                {
                    dr[1  + d + i] = row.Metrics[i];
                }
                
                table.Rows.Add(dr);
            }
        }
        internal string GetTableName(MetricData data)
        {
            return string.Format("MetricData_{0}_{1}", data.Dimensions.Count(), data.Metrics.Count());
        }
        internal string GetViewName(Metric metric)
        {
            return string.Format("MetricData_{0}_View", metric.Id);
        }
        private async Task CreateViewAsync(Metric metric, MetricData data)
        {
            using (var cnn = new SqlConnection(Utility.ConnectionString("CentralizedAnalyticsDb")))
            {
                await cnn.OpenAsync();
                string viewName = GetViewName(metric);

                if(await DbObjectExists(cnn, viewName))
                {
                    return;
                }
                string viewColumns = String.Join(",\r\n",
                        data.Dimensions.Select((name, i) => string.Format("[dimension{0}] [{1}]", i, name))
                        .Union(
                            data.Metrics.Select((name, i) => string.Format("[metric{0}] [{1}]", i, name)
                            )
                         )
                    );
                var viewCmd = new SqlCommand()
                {
                    Connection = cnn,
                    CommandType = CommandType.Text,
                    CommandText = string.Format(
                        Core.Properties.Resources.CreateMetricDataView,
                        GetTableName(data),
                        viewName,
                        viewColumns,
                        metric.Id
                        )
                };
                Utility.Logger.Info(viewCmd.CommandText);
                await viewCmd.ExecuteNonQueryAsync();
            }


        }
        private async Task<bool> DbObjectExists(SqlConnection cnn,string name)
        {
            SqlCommand cmd = new SqlCommand()
            {
                CommandText = Core.Properties.Resources.TableExistsQuery,
                CommandType = CommandType.Text,
                Connection = cnn

            };
            cmd.Parameters.Add(new SqlParameter("@TableName", "dbo." + name ));

            Utility.Logger.Debug(cmd.CommandText);
            var exists = (int)await cmd.ExecuteScalarAsync();

            return exists == 1;
        }
        private async Task CreateTableAsync(Metric metric, MetricData data)
        {
            using (var cnn = new SqlConnection(Utility.ConnectionString("CentralizedAnalyticsDb")))
            {
                string tableName = GetTableName(data);
                await cnn.OpenAsync();

                if(await DbObjectExists(cnn, tableName))
                {
                    return;
                }
                
                var columns = String.Join(",\r\n", 
                    data.Dimensions
                        .Select((name, i) => string.Format("[dimension{0}] varchar(255) not null", i))
                        .Union(
                            data.Metrics.Select((name, i) => string.Format("[metric{0}] float not null", i))
                        )
                    );

                

                string cmdStr =
                     string.Format(
                         Core.Properties.Resources.CreateMetricDataTable,
                         GetTableName(data),
                         columns
                         );
                SqlCommand createCmd = new SqlCommand()
                {
                    Connection = cnn,
                    CommandType = CommandType.Text
                };
                foreach(var batch in cmdStr.Split(new []{"GO" }, StringSplitOptions.RemoveEmptyEntries)){
                    Utility.Logger.Info(createCmd.CommandText);
                    createCmd.CommandText = batch;
                    await createCmd.ExecuteNonQueryAsync();
                }
                
                

                

            }
        }
    }
}
