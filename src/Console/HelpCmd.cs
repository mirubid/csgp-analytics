﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics
{
    class HelpCmd : ICmd
    {
        private IEnumerable<ICmd> _commands=null;

        public string[] Names
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        HelpCmd(IEnumerable<ICmd> commands)
        {
            _commands = commands;
        }
        public void ConfigureOptions(ICmdConfigurator options)
        {
            throw new NotImplementedException();
        }

        public int Execute(List<string> extraArgs)
        {
            throw new NotImplementedException();
        }
        internal void WriteHelp(OptionParser options)
        {
            if(_commands!=null)
            {
                Console.WriteLine("available commands:");
                _commands
                    .Select(e => String.Join(",", e.Names))
                    .ToList()
                    .ForEach(Console.WriteLine);

            }
            options.WriteHelp(Console.Out);
        }
    }
}
