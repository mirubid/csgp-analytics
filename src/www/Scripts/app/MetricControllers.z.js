﻿(function () {
    'use strict';

    angular.module('Metric', ['ngRoute'])
        .controller('TenantDataSource_Metric_list', ['$scope','$rootScope', '$http', function ($scope,$rootScope, $http, $routeParams) {
            $scope.data = [];
            
            function getMetrics(metric) {
                if (!metric) {
                    metric = {
                        TenantId: $scope.tenantId,
                        DataSourceId: $scope.tenantDataSourceId
                    }
                }
                $scope.tenantDataSourceId = metric.DataSourceId;
                $scope.tenantId = metric.TenantId;

                $http.get('/Api/Metric/', { params: metric })
                .then(function (response) { $scope.data = response.data; });
            }
            
            $rootScope.$on('TenantDataSource_Metrics::Show', function (ev,metric) {
                               
                getMetrics(metric);
            });
            $rootScope.$on('TenantDataSource_Metrics::Create', function (ev, metric) {
                
                $scope.dataSourceTypeId = metric.DataSourceTypeId;
                $scope.showMetricEditDialog = !!metric.DataSourceTypeId;
                getMetrics(metric);
            });
            $rootScope.$on('TenantDataSource_Metrics::Saved', function (ev, metric) {
                $scope.showMetricEditDialog = false;
                getMetrics();
            });
            $scope.$on('TenantDataSource_Metrics::CancelCreate', function (ev, metric) {
                
                $scope.showMetricEditDialog = false;
            });
        }])
        .controller('TenantDataSource_Metric_edit', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location) {

            $scope.$parent.$watchGroup(['tenantDataSourceId', 'tenantId'], function (newValues, oldValues) {
                $scope.data.TenantId = newValues[1];
                $scope.data.DataSourceId = newValues[0];
                loadMetric();
            });

            $scope.data = {
                TenantId: $scope.$parent.tenantId,
                DataSourceId: $scope.$parent.tenantDataSourceId,
                Configuration:''

            };

            function loadMetric() {
                $http.get('/Api/TenantDataSource/', { params: { TenantId: $scope.data.TenantId, Direction: 2 } })
                    .then(function (response) { $scope.DataDestination_options = response.data; });
            }
            //$http.get('/Api/Metric/' + $routeParams.id)
            //.then(function (response) { $scope.data = response.data; });



            $scope.save = function () {
                var req;
                if ($scope.data.Id) {
                    req=$http.put('/Api/Metric/' + $scope.data.Id, $scope.data)
                } else {
                    req=$http.post('/Api/Metric/', $scope.data)
                }

                
                req.then(function (response) {
                    console.log('TenantDataSource_Metrics::Saved', response);
                    $scope.$emit('TenantDataSource_Metrics::Saved', response);
                });
            }

            $scope.cancel = function () {
                $scope.$emit('TenantDataSource_Metrics::CancelCreate', {});
            }
            
        }])
    .controller('Metric_edit_GA', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location) {
        $scope.data={Query:''}
        $scope.$watch('data.Query', function (newValue) {
            console.log('Query', arguments);
            $scope.$parent.data.Configuration = newValue;
        },true);

    }])
;

})();
