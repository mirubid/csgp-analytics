﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace csgp.CentralizedAnalytics.Tasks
{
    public interface IRecurringTaskScheduler
    {
        void AddOrUpdate<T>(string taskId,Expression<Action<T>> expression, string schedule);
        void RemoveIfExists(string taskId);
    }
}
