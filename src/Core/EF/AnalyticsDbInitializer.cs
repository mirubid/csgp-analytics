﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace csgp.CentralizedAnalytics.EF
{
    public class AnalyticsDbInitializer:DropCreateDatabaseIfModelChanges<AnalyticsDbContext>
    {

        protected override void Seed(AnalyticsDbContext context)
        {
            Seeder.Seed(context);

            base.Seed(context);
        }
    }
}
