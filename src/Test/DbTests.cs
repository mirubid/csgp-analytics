﻿using System;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;

namespace csgp.CentralizedAnalytics
{
    [TestClass]
    public class DbTests
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            using (var db = new EF.AnalyticsDbContext())
            {
                if (!db.Tenants.Any(e => e.Id == "Test"))
                {
                    EF.Seeder.Seed(db);                    
                }
                db.SaveChanges();
            }
        }
        [TestMethod]
        public void ShouldBeAbleToCreateATenant()
        {
            EF.AnalyticsDbContext db;

            Tenant tenant;
            using (db= new EF.AnalyticsDbContext()) {

                tenant = db.Tenants.Create();

                tenant.Description = "a tenant for the purpose of testing";

                tenant.Id = "ut-" + Path.GetRandomFileName();

                db.Tenants.Add(tenant);

                db.SaveChanges();
            }
            using (db = new EF.AnalyticsDbContext())
            {

                var actual = db.Tenants.Find(tenant.Id);
                Assert.IsNotNull(actual);

                Assert.IsNotNull(actual.Description);
            }
        }
        [TestMethod]
        public void ShouldBeAbleToGetDataSourceTypes()
        {
            using (var db = new EF.AnalyticsDbContext())
            {
                var sources_count = db.DataSourceTypes.Count();
                Assert.AreNotEqual(0, sources_count,"sources count");
            }
        }

        [TestMethod]
        public void ShouldBeAbleToAddATenantDataSource()
        {
            string id = Path.GetRandomFileName();
            string tenant_id;
            using (var db = new EF.AnalyticsDbContext())
            {
                var tenant=db.Tenants.First(e=>e.Id=="Test");
                Assert.IsNotNull(tenant, "tenant");
                tenant_id = tenant.Id;

                tenant.DataSources.Add(new TenantDataSource() { Id = id, TenantId = tenant.Id, DataSourceTypeId = "GA",Configuration="{}" });

                db.SaveChanges();
            }
            using(var db=new EF.AnalyticsDbContext())
            {
                var source = db.TenantDataSources.Where(e => e.Id == id && e.TenantId == tenant_id && e.DataSourceTypeId == "GA")
                    .FirstOrDefault();

                Assert.IsNotNull(source);
                
            }
        }
        [TestMethod]
        public void ShouldBeAbleToDeleteATenantDataSource()
        {
            string id = Path.GetRandomFileName();
            string tenant_id;
            using (var db = new EF.AnalyticsDbContext())
            {
                var tenant = db.Tenants.First();
                Assert.IsNotNull(tenant, "tenant");
                tenant_id = tenant.Id;

                tenant.DataSources.Add(new TenantDataSource() { Id = id, TenantId = tenant.Id, DataSourceTypeId = "GA", Configuration = "{}" });

                db.SaveChanges();
            }
            using (var db = new EF.AnalyticsDbContext())
            {
                var source = db.TenantDataSources.Where(e => e.Id == id && e.TenantId == tenant_id && e.DataSourceTypeId == "GA")
                    .FirstOrDefault();

                Assert.IsNotNull(source);

                db.TenantDataSources.Remove(source);
                db.SaveChanges();
            }
            using (var db = new EF.AnalyticsDbContext())
            {
                var exists = db.TenantDataSources.Where(e => e.Id == id && e.TenantId == tenant_id && e.DataSourceTypeId == "GA")
                    .Any();

                Assert.IsFalse(exists);
            }
        }
        [TestMethod]
        public void ShouldBeAbleToCreateGAMetric()
        {
            Tenant tenant;
            string id = Path.GetRandomFileName();

            using (var db = new EF.AnalyticsDbContext())
            {
                tenant = db.Tenants.Include("DataSources").First(e=>e.Id=="Test");
                
                if(!tenant.DataSources.Any(e => e.DataSourceTypeId == "GA"))
                {
                    ShouldBeAbleToAddATenantDataSource();
                    db.Entry(tenant).Reference(e => e.DataSources).Load();
                }

                tenant.DataSources.AssertAny("DataSources");

                var ds=tenant.DataSources.First(e => e.DataSourceTypeId == "GA");


                ds.Metrics.Add(new Metric() { Description = "metric for testing", Name = "test metric", DataDestinationId="NULL Test" });
                db.SaveChanges();
            }
        }
        [TestMethod]
        public void ShouldBeAbleToCreateADataPoint()
        {
            Tenant tenant;
            Metric metric;
            DateTime now = new DateTime(2001, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            using (var db = new EF.AnalyticsDbContext())
            {
                tenant = db.Tenants.First(e => e.Id == "Test");

                metric=db.Metrics.Where(e => e.TenantId == tenant.Id && e.DataSourceId == "GA Test").FirstOrDefault();

                var dp = new MetricDataPoint() { MetricId = metric.Id, QueryStartDate =  "2daysago".ParseAsDate(now), QueryEndDate ="2daysago".ParseAsDate(now) };
                db.MetricDataPoints.Add(dp);
                
                db.SaveChanges();
                Assert.AreNotEqual(0, dp.Id, @" 0 vs dp.Id");

            }
        }
        [TestMethod]
        public async Task ShouldBeAbleToSaveMetricDatapointsWithSaver()
        {
            IMetricDataPointSaver saver = new EF.MetricDatapointSaverForEF();
            Tenant tenant;
            Metric metric;
            DateTime now = new DateTime(2001, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            using (var db = new EF.AnalyticsDbContext())
            {
                metric = db.Metrics.Include("DataDestination").Where(e => e.TenantId =="Test" && e.DataDestinationId == "LocalDb Test").FirstOrDefault();

            }
            await saver.SaveAsync( metric,
                new MetricDataPoint()
                {
                    MetricId = metric.Id,
                    QueryStartDate = string.Format("{0}daysago", 1).ParseAsDate(now),
                    QueryEndDate = string.Format("{0}daysago", 1).ParseAsDate(now)
                },
                new MetricData()
                {
                    Dimensions=new List<string>(),
                    Metrics=new List<string>() { "metric"},
                    Rows= Enumerable.Range(1, 5).Select(
                        e => new MetricDataRow()
                        {
                            Dimensions=new List<string>(),
                            Metrics=new List<double>() { (double)e}
                        }
                    ).ToList()
                }
                
            );
        }
        [TestMethod]
        public async Task ShouldBeAbleToSaveMultiValueResult()
        {
            var saver = new MetricDataSaverDb();
            var data = new MetricData()
            {
                Dimensions = new List<string>() { "ga:blah", "test:dimension" },
                Metrics = new List<string>() { "measured thing 1", "another measured thing" },
                Rows = new List<MetricDataRow>()
            };
            Tenant tenant;
            Metric metric;
            var now = DateTime.Now;

            using (var db = new EF.AnalyticsDbContext())
            {
                
                tenant = db.Tenants.First(e => e.Id == "Test");

                metric = db.Metrics.Where(e => e.TenantId == tenant.Id && e.DataSourceId == "GA Test").FirstOrDefault();

                

            }
            Assert.IsNotNull(metric, "metric");

            await saver.SaveAsync(metric,new MetricDataPoint(), data);
        }
        //[TestMethod]
        public void ShouldBeAbleToDoAMigration()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EF.AnalyticsDbContext, Migrations.Configuration>());

            using (var db = new EF.AnalyticsDbContext())
            {
                var metrics = db.Metrics.Count();

            }
                
        }
        //[TestMethod]
        public void ShouldBeAbleToDownGrade()
        {
            
            var version = new Migrations.PageIndex();
            version.Down();

        }
    }
}
