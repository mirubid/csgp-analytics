﻿ var  modules = modules || [];
(function () {
    'use strict';
    modules.push('Metric');

    angular.module('Metric',['ngRoute'])
    .controller('Metric_list', ['$scope', '$http', function($scope, $http,$routeParams){
        console.log('$routeParams', $routeParams);
        $http.get('/Api/Metric/')
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('Metric_details', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

        $http.get('/Api/Metric/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('Metric_create', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){
        console.log('$routeParams', $routeParams);
        $scope.data = {};
                $http.get('/Api/TenantDataSource/')
        .then(function (response) { $scope.TenantId_options = response.data; });

                $http.get('/Api/TenantDataSource/')
        .then(function(response){$scope.TenantId_options = response.data;});
        
        $scope.save = function(){
            $http.post('/Api/Metric/', $scope.data)
            .then(function(response){ $location.path("Metric"); });
        }

    }])
    .controller('Metric_edit', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/Metric/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

                $http.get('/Api/TenantDataSource/')
        .then(function(response){$scope.TenantId_options = response.data;});
                $http.get('/Api/TenantDataSource/')
        .then(function(response){$scope.TenantId_options = response.data;});
        
        $scope.save = function(){
            $http.put('/Api/Metric/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("Metric"); });
        }

    }])
    .controller('Metric_delete', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/Metric/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});
        $scope.save = function(){
            $http.delete('/Api/Metric/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("Metric"); });
        }

    }])

    .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
            .when('/Metric', {
                title: 'Metric - List',
                templateUrl: '/Static/Metric_List',
                controller: 'Metric_list'
            })
            .when('/Metric/Create', {
                title: 'Metric - Create',
                templateUrl: '/Static/Metric_Edit',
                controller: 'Metric_create'
            })
            .when('/Metric/Edit/:id', {
                title: 'Metric - Edit',
                templateUrl: '/Static/Metric_Edit',
                controller: 'Metric_edit'
            })
            .when('/Metric/Delete/:id', {
                title: 'Metric - Delete',
                templateUrl: '/Static/Metric_Delete',
                controller: 'Metric_delete'
            })
            .when('/Metric/:id', {
                title: 'Metric - Details',
                templateUrl: '/Static/Metric_Details',
                controller: 'Metric_details'
            })
    }])
;

})();
